﻿Public Class objectosavali

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        help.Show()
    End Sub

    Private Sub objectosavali_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Area' table. You can move, or remove it, as needed.
        Me.AreaTableAdapter.Fill(Me.DossieProjectoDataSet.Area)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.AvaliArea' table. You can move, or remove it, as needed.
        Me.AvaliAreaTableAdapter.Fill(Me.DossieProjectoDataSet.AvaliArea)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Objecto' table. You can move, or remove it, as needed.
        Me.ObjectoTableAdapter.Fill(Me.DossieProjectoDataSet.Objecto)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Avaliacao' table. You can move, or remove it, as needed.
        Me.AvaliacaoTableAdapter.Fill(Me.DossieProjectoDataSet.Avaliacao)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Objecto' table. You can move, or remove it, as needed.
        Me.ObjectoTableAdapter.Fill(Me.DossieProjectoDataSet.Objecto)

        Me.AreaTableAdapter.FillBy(Me.DossieProjectoDataSet.Area, ComboBox1.SelectedValue)
        Me.ObjectoTableAdapter.FillBy(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue, AreaCombo.SelectedValue)

    End Sub



    Private Sub ObjectoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.ObjectoBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)

    End Sub

    Private Sub GroupBox2_Enter(sender As Object, e As EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try

            If (NomeTextBox.Text = "") Then
                MessageBox.Show("Nome não pode ser vazio", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Try
            End If

            If (Not IsNumeric(NumericUpDown1.Value)) Then
                MessageBox.Show("Percentagem percisa e ser numerica", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Try
            End If


            Me.ObjectoTableAdapter.InsertQuery(ComboBox1.SelectedValue, AreaCombo.SelectedValue, NomeTextBox.Text, NumericUpDown1.Value)
            Me.ObjectoTableAdapter.FillBy(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue, AreaCombo.SelectedValue)
            MessageBox.Show("Objecto adicionado a avaliação!", "Sucesso", MessageBoxButtons.OK)

            AreaCombo.Text = ""
            NomeTextBox.Text = ""
            NumericUpDown1.Value = 0
        Catch ex As Exception
            MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        Me.ObjectoTableAdapter.FillBy(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue, ListBox1.SelectedValue)
    End Sub

    Private Sub AreaCombo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles AreaCombo.SelectedIndexChanged
        Me.ObjectoTableAdapter.FillBy(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue, AreaCombo.SelectedValue)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Me.AreaTableAdapter.FillBy(Me.DossieProjectoDataSet.Area, ComboBox1.SelectedValue)
        Me.ObjectoTableAdapter.FillBy(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue, AreaCombo.SelectedValue)
    End Sub

    Private Sub objectosavali_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If (avaliacao.codavaliacao_escolhida <> 0) Then
            ComboBox1.SelectedValue = avaliacao.codavaliacao_escolhida
        End If
    End Sub

    Private Sub ObjectoDataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ObjectoDataGridView.CellContentClick
        If e.ColumnIndex = 4 Then

            Dim id As Integer = ObjectoDataGridView.Rows(e.RowIndex).Cells(0).Value
            Dim result = MessageBox.Show("Deseja mesmo eleminar este objecto?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If (result = DialogResult.Yes) Then
                Try
                    Me.ObjectoTableAdapter.DeleteQuery(id)
                    Me.ObjectoTableAdapter.Fill(Me.DossieProjectoDataSet.Objecto)
                    Me.Validate()
                    Me.AvaliacaoBindingSource.EndEdit()
                Catch ex As Exception
                    MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        End If
    End Sub
End Class