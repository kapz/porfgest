﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TurmaAlunos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NomeLabel As System.Windows.Forms.Label
        Dim NumeroLabel As System.Windows.Forms.Label
        Dim EmailLabel As System.Windows.Forms.Label
        Dim TelefoneLabel As System.Windows.Forms.Label
        Dim TelEncarregadoLabel As System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TelEncarregadoTextBox = New System.Windows.Forms.TextBox()
        Me.AlunoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DossieProjectoDataSet = New Profgest.DossieProjectoDataSet()
        Me.TelefoneTextBox = New System.Windows.Forms.TextBox()
        Me.EmailTextBox = New System.Windows.Forms.TextBox()
        Me.NumeroTextBox = New System.Windows.Forms.TextBox()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.turma = New System.Windows.Forms.ComboBox()
        Me.TurmaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TurmaFormatedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AlunoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AlunoTableAdapter()
        Me.TableAdapterManager = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.TurmaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.TurmaTableAdapter()
        Me.TurmaFormatedTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter()
        Me.AlunoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        NomeLabel = New System.Windows.Forms.Label()
        NumeroLabel = New System.Windows.Forms.Label()
        EmailLabel = New System.Windows.Forms.Label()
        TelefoneLabel = New System.Windows.Forms.Label()
        TelEncarregadoLabel = New System.Windows.Forms.Label()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.AlunoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlunoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NomeLabel
        '
        NomeLabel.AutoSize = True
        NomeLabel.Font = New System.Drawing.Font("Open Sans Light", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NomeLabel.Location = New System.Drawing.Point(103, 187)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(69, 26)
        NomeLabel.TabIndex = 40
        NomeLabel.Text = "Nome:"
        '
        'NumeroLabel
        '
        NumeroLabel.AutoSize = True
        NumeroLabel.Font = New System.Drawing.Font("Open Sans Light", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        NumeroLabel.Location = New System.Drawing.Point(54, 141)
        NumeroLabel.Name = "NumeroLabel"
        NumeroLabel.Size = New System.Drawing.Size(118, 26)
        NumeroLabel.TabIndex = 41
        NumeroLabel.Text = "Nº do Aluno:"
        '
        'EmailLabel
        '
        EmailLabel.AutoSize = True
        EmailLabel.Font = New System.Drawing.Font("Open Sans Light", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        EmailLabel.Location = New System.Drawing.Point(110, 233)
        EmailLabel.Name = "EmailLabel"
        EmailLabel.Size = New System.Drawing.Size(62, 26)
        EmailLabel.TabIndex = 42
        EmailLabel.Text = "Email:"
        '
        'TelefoneLabel
        '
        TelefoneLabel.AutoSize = True
        TelefoneLabel.Font = New System.Drawing.Font("Open Sans Light", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TelefoneLabel.Location = New System.Drawing.Point(70, 279)
        TelefoneLabel.Name = "TelefoneLabel"
        TelefoneLabel.Size = New System.Drawing.Size(102, 26)
        TelefoneLabel.TabIndex = 43
        TelefoneLabel.Text = "Telemovel:"
        '
        'TelEncarregadoLabel
        '
        TelEncarregadoLabel.AutoSize = True
        TelEncarregadoLabel.Font = New System.Drawing.Font("Open Sans Light", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        TelEncarregadoLabel.Location = New System.Drawing.Point(15, 324)
        TelEncarregadoLabel.Name = "TelEncarregadoLabel"
        TelEncarregadoLabel.Size = New System.Drawing.Size(157, 26)
        TelEncarregadoLabel.TabIndex = 44
        TelEncarregadoLabel.Text = "Tel. Encarregado:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(192, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(174, 65)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Alunos"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Open Sans Light", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(74, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 39)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Profgest"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.turmas
        Me.PictureBox3.Location = New System.Drawing.Point(13, 65)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 34
        Me.PictureBox3.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(13, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 30
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(1, -1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(987, 57)
        Me.PictureBox2.TabIndex = 31
        Me.PictureBox2.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.PictureBox4.Location = New System.Drawing.Point(0, 57)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 35
        Me.PictureBox4.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox7.Location = New System.Drawing.Point(186, 57)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(802, 97)
        Me.PictureBox7.TabIndex = 37
        Me.PictureBox7.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.White
        Me.Label3.Font = New System.Drawing.Font("Open Sans", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(78, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 33)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Turma:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(TelEncarregadoLabel)
        Me.Panel1.Controls.Add(Me.TelEncarregadoTextBox)
        Me.Panel1.Controls.Add(TelefoneLabel)
        Me.Panel1.Controls.Add(Me.TelefoneTextBox)
        Me.Panel1.Controls.Add(EmailLabel)
        Me.Panel1.Controls.Add(Me.EmailTextBox)
        Me.Panel1.Controls.Add(NumeroLabel)
        Me.Panel1.Controls.Add(Me.NumeroTextBox)
        Me.Panel1.Controls.Add(NomeLabel)
        Me.Panel1.Controls.Add(Me.NomeTextBox)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.turma)
        Me.Panel1.Location = New System.Drawing.Point(14, 166)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(961, 594)
        Me.Panel1.TabIndex = 44
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Open Sans", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(503, 271)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(85, 86)
        Me.Button2.TabIndex = 51
        Me.Button2.Text = "Novo"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Open Sans", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.Location = New System.Drawing.Point(594, 133)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(100, 40)
        Me.Button4.TabIndex = 50
        Me.Button4.Text = "<"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Open Sans", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(700, 133)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(100, 40)
        Me.Button5.TabIndex = 49
        Me.Button5.Text = ">"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.CornflowerBlue
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Image = Global.Profgest.My.Resources.Resources.registo
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(594, 271)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(206, 86)
        Me.Button1.TabIndex = 46
        Me.Button1.Text = "Adicionar Aluno"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TelEncarregadoTextBox
        '
        Me.TelEncarregadoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TelEncarregadoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AlunoBindingSource, "telEncarregado", True))
        Me.TelEncarregadoTextBox.Font = New System.Drawing.Font("Open Sans Light", 18.0!)
        Me.TelEncarregadoTextBox.ForeColor = System.Drawing.Color.DarkGray
        Me.TelEncarregadoTextBox.Location = New System.Drawing.Point(205, 317)
        Me.TelEncarregadoTextBox.Name = "TelEncarregadoTextBox"
        Me.TelEncarregadoTextBox.Size = New System.Drawing.Size(282, 40)
        Me.TelEncarregadoTextBox.TabIndex = 45
        '
        'AlunoBindingSource
        '
        Me.AlunoBindingSource.DataMember = "Aluno"
        Me.AlunoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'DossieProjectoDataSet
        '
        Me.DossieProjectoDataSet.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TelefoneTextBox
        '
        Me.TelefoneTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TelefoneTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AlunoBindingSource, "telefone", True))
        Me.TelefoneTextBox.Font = New System.Drawing.Font("Open Sans Light", 18.0!)
        Me.TelefoneTextBox.Location = New System.Drawing.Point(205, 271)
        Me.TelefoneTextBox.Name = "TelefoneTextBox"
        Me.TelefoneTextBox.Size = New System.Drawing.Size(282, 40)
        Me.TelefoneTextBox.TabIndex = 44
        '
        'EmailTextBox
        '
        Me.EmailTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.EmailTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AlunoBindingSource, "email", True))
        Me.EmailTextBox.Font = New System.Drawing.Font("Open Sans Light", 18.0!)
        Me.EmailTextBox.ForeColor = System.Drawing.Color.DarkGray
        Me.EmailTextBox.Location = New System.Drawing.Point(205, 225)
        Me.EmailTextBox.Name = "EmailTextBox"
        Me.EmailTextBox.Size = New System.Drawing.Size(512, 40)
        Me.EmailTextBox.TabIndex = 43
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NumeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AlunoBindingSource, "numero", True))
        Me.NumeroTextBox.Font = New System.Drawing.Font("Open Sans Light", 18.0!)
        Me.NumeroTextBox.ForeColor = System.Drawing.Color.DarkGray
        Me.NumeroTextBox.Location = New System.Drawing.Point(205, 133)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(84, 40)
        Me.NumeroTextBox.TabIndex = 42
        Me.NumeroTextBox.Text = "1"
        '
        'NomeTextBox
        '
        Me.NomeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.NomeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AlunoBindingSource, "nome", True))
        Me.NomeTextBox.Font = New System.Drawing.Font("Open Sans Light", 18.0!)
        Me.NomeTextBox.Location = New System.Drawing.Point(205, 179)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(595, 40)
        Me.NomeTextBox.TabIndex = 41
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label8.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(295, 129)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(0, 65)
        Me.Label8.TabIndex = 40
        '
        'turma
        '
        Me.turma.BackColor = System.Drawing.Color.LightGray
        Me.turma.DataBindings.Add(New System.Windows.Forms.Binding("SelectedItem", Me.TurmaBindingSource, "letra", True))
        Me.turma.DataSource = Me.TurmaFormatedBindingSource
        Me.turma.DisplayMember = "TurmaData"
        Me.turma.DropDownHeight = 130
        Me.turma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.turma.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.turma.Font = New System.Drawing.Font("Open Sans", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.turma.ForeColor = System.Drawing.Color.DimGray
        Me.turma.FormattingEnabled = True
        Me.turma.IntegralHeight = False
        Me.turma.Location = New System.Drawing.Point(189, 36)
        Me.turma.MaxDropDownItems = 4
        Me.turma.Name = "turma"
        Me.turma.Size = New System.Drawing.Size(611, 36)
        Me.turma.TabIndex = 31
        Me.turma.ValueMember = "codTurma"
        '
        'TurmaBindingSource
        '
        Me.TurmaBindingSource.DataMember = "Turma"
        Me.TurmaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'TurmaFormatedBindingSource
        '
        Me.TurmaFormatedBindingSource.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'AlunoTableAdapter
        '
        Me.AlunoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager.AlunoTableAdapter = Me.AlunoTableAdapter
        Me.TableAdapterManager.AreaTableAdapter = Nothing
        Me.TableAdapterManager.AvaliacaoTableAdapter = Nothing
        Me.TableAdapterManager.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DisciplinaTableAdapter = Nothing
        Me.TableAdapterManager.ModuloTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoTableAdapter = Nothing
        Me.TableAdapterManager.SumarioTableAdapter = Nothing
        Me.TableAdapterManager.TurmaFormatedTableAdapter = Nothing
        Me.TableAdapterManager.TurmaTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'TurmaTableAdapter
        '
        Me.TurmaTableAdapter.ClearBeforeFill = True
        '
        'TurmaFormatedTableAdapter
        '
        Me.TurmaFormatedTableAdapter.ClearBeforeFill = True
        '
        'AlunoBindingSource1
        '
        Me.AlunoBindingSource1.DataMember = "TurmaAluno"
        Me.AlunoBindingSource1.DataSource = Me.TurmaBindingSource
        '
        'TurmaAlunos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(833, 545)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox7)
        Me.Name = "TurmaAlunos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Alunos"
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.AlunoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlunoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents turma As System.Windows.Forms.ComboBox
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents AlunoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AlunoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AlunoTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TelEncarregadoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TelefoneTextBox As System.Windows.Forms.TextBox
    Friend WithEvents EmailTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents TurmaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TurmaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.TurmaTableAdapter
    Friend WithEvents TurmaFormatedBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TurmaFormatedTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter
    Friend WithEvents AlunoBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Button2 As System.Windows.Forms.Button
End Class
