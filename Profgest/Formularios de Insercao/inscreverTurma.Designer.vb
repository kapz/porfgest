﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class inscreverTurma
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.OvalShape1 = New Microsoft.VisualBasic.PowerPacks.OvalShape()
        Me.letra = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.curso = New System.Windows.Forms.TextBox()
        Me.ano = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.nAlunos = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.su2 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.ShapeContainer2 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.DossieProjectoDataSet1 = New Profgest.DossieProjectoDataSet()
        Me.TurmaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TurmaTableAdapter1 = New Profgest.DossieProjectoDataSetTableAdapters.TurmaTableAdapter()
        Me.TableAdapterManager1 = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.s4 = New System.Windows.Forms.Button()
        Me.su1 = New System.Windows.Forms.PictureBox()
        Me.su3 = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        CType(Me.DossieProjectoDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.su1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label2.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(190, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(289, 65)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Nova Turma"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Open Sans Light", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(72, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 39)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Profgest"
        '
        'OvalShape1
        '
        Me.OvalShape1.BorderColor = System.Drawing.Color.White
        Me.OvalShape1.FillColor = System.Drawing.Color.DeepSkyBlue
        Me.OvalShape1.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid
        Me.OvalShape1.Location = New System.Drawing.Point(26, 29)
        Me.OvalShape1.Name = "OvalShape1"
        Me.OvalShape1.Size = New System.Drawing.Size(250, 250)
        '
        'letra
        '
        Me.letra.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.letra.DropDownHeight = 130
        Me.letra.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.letra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.letra.Font = New System.Drawing.Font("Open Sans Light", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.letra.ForeColor = System.Drawing.Color.White
        Me.letra.FormattingEnabled = True
        Me.letra.IntegralHeight = False
        Me.letra.Items.AddRange(New Object() {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "X", "Z"})
        Me.letra.Location = New System.Drawing.Point(112, 137)
        Me.letra.MaxDropDownItems = 4
        Me.letra.Name = "letra"
        Me.letra.Size = New System.Drawing.Size(86, 59)
        Me.letra.TabIndex = 31
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Open Sans Light", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(334, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 33)
        Me.Label3.TabIndex = 33
        Me.Label3.Text = "Ano"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Label4.Font = New System.Drawing.Font("Open Sans Light", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(119, 80)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(70, 33)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Letra"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Open Sans Light", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(548, 54)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(24, 33)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "º"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Open Sans Light", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(334, 141)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(78, 33)
        Me.Label6.TabIndex = 36
        Me.Label6.Text = "Curso"
        '
        'curso
        '
        Me.curso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.curso.Font = New System.Drawing.Font("Open Sans Light", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.curso.ForeColor = System.Drawing.Color.DarkGray
        Me.curso.Location = New System.Drawing.Point(461, 139)
        Me.curso.Name = "curso"
        Me.curso.Size = New System.Drawing.Size(480, 40)
        Me.curso.TabIndex = 37
        '
        'ano
        '
        Me.ano.BackColor = System.Drawing.Color.White
        Me.ano.DropDownHeight = 130
        Me.ano.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.ano.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ano.Font = New System.Drawing.Font("Open Sans Light", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ano.ForeColor = System.Drawing.Color.DimGray
        Me.ano.FormattingEnabled = True
        Me.ano.IntegralHeight = False
        Me.ano.Items.AddRange(New Object() {"7", "8", "9", "10", "11", "12"})
        Me.ano.Location = New System.Drawing.Point(461, 54)
        Me.ano.MaxDropDownItems = 4
        Me.ano.Name = "ano"
        Me.ano.Size = New System.Drawing.Size(86, 59)
        Me.ano.TabIndex = 32
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Open Sans Light", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(334, 211)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(119, 33)
        Me.Label7.TabIndex = 38
        Me.Label7.Text = "Nº Alunos"
        '
        'nAlunos
        '
        Me.nAlunos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.nAlunos.Font = New System.Drawing.Font("Open Sans Light", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nAlunos.ForeColor = System.Drawing.Color.DarkGray
        Me.nAlunos.Location = New System.Drawing.Point(461, 209)
        Me.nAlunos.Name = "nAlunos"
        Me.nAlunos.Size = New System.Drawing.Size(85, 40)
        Me.nAlunos.TabIndex = 39
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Label8.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label8.Location = New System.Drawing.Point(295, 129)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(0, 65)
        Me.Label8.TabIndex = 40
        '
        'su2
        '
        Me.su2.AutoSize = True
        Me.su2.BackColor = System.Drawing.Color.White
        Me.su2.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.su2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.su2.Location = New System.Drawing.Point(169, 454)
        Me.su2.Name = "su2"
        Me.su2.Size = New System.Drawing.Size(639, 65)
        Me.su2.TabIndex = 41
        Me.su2.Text = "Turma Inserida com sucesso"
        Me.su2.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.nAlunos)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.curso)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.ano)
        Me.Panel1.Controls.Add(Me.letra)
        Me.Panel1.Controls.Add(Me.ShapeContainer2)
        Me.Panel1.Location = New System.Drawing.Point(11, 162)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(960, 484)
        Me.Panel1.TabIndex = 43
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Image = Global.Profgest.My.Resources.Resources.registo
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(583, 384)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(358, 88)
        Me.Button5.TabIndex = 29
        Me.Button5.Text = "Inscrever Turma"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button5.UseVisualStyleBackColor = False
        '
        'ShapeContainer2
        '
        Me.ShapeContainer2.AutoScroll = True
        Me.ShapeContainer2.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer2.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer2.Name = "ShapeContainer2"
        Me.ShapeContainer2.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.OvalShape1})
        Me.ShapeContainer2.Size = New System.Drawing.Size(960, 484)
        Me.ShapeContainer2.TabIndex = 41
        Me.ShapeContainer2.TabStop = False
        '
        'DossieProjectoDataSet1
        '
        Me.DossieProjectoDataSet1.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TurmaBindingSource
        '
        Me.TurmaBindingSource.DataMember = "Turma"
        Me.TurmaBindingSource.DataSource = Me.DossieProjectoDataSet1
        '
        'TurmaTableAdapter1
        '
        Me.TurmaTableAdapter1.ClearBeforeFill = True
        '
        'TableAdapterManager1
        '
        Me.TableAdapterManager1.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager1.AlunoTableAdapter = Nothing
        Me.TableAdapterManager1.AreaTableAdapter = Nothing
        Me.TableAdapterManager1.AvaliacaoTableAdapter = Nothing
        Me.TableAdapterManager1.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager1.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager1.DisciplinaTableAdapter = Nothing
        Me.TableAdapterManager1.ModuloTableAdapter = Nothing
        Me.TableAdapterManager1.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager1.ObjectoTableAdapter = Nothing
        Me.TableAdapterManager1.SumarioTableAdapter = Nothing
        Me.TableAdapterManager1.TurmaFormatedTableAdapter = Nothing
        Me.TableAdapterManager1.TurmaTableAdapter = Me.TurmaTableAdapter1
        Me.TableAdapterManager1.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        's4
        '
        Me.s4.BackColor = System.Drawing.Color.CornflowerBlue
        Me.s4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.s4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.s4.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.s4.ForeColor = System.Drawing.Color.White
        Me.s4.Image = Global.Profgest.My.Resources.Resources.registo
        Me.s4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.s4.Location = New System.Drawing.Point(500, 530)
        Me.s4.Name = "s4"
        Me.s4.Size = New System.Drawing.Size(306, 52)
        Me.s4.TabIndex = 44
        Me.s4.Text = "Adicionar Alunos"
        Me.s4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.s4.UseVisualStyleBackColor = False
        Me.s4.Visible = False
        '
        'su1
        '
        Me.su1.Image = Global.Profgest.My.Resources.Resources.sucess
        Me.su1.Location = New System.Drawing.Point(414, 253)
        Me.su1.Name = "su1"
        Me.su1.Size = New System.Drawing.Size(150, 150)
        Me.su1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.su1.TabIndex = 42
        Me.su1.TabStop = False
        Me.su1.Visible = False
        '
        'su3
        '
        Me.su3.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.su3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.su3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.su3.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.su3.ForeColor = System.Drawing.Color.White
        Me.su3.Image = Global.Profgest.My.Resources.Resources.registo
        Me.su3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.su3.Location = New System.Drawing.Point(171, 529)
        Me.su3.Name = "su3"
        Me.su3.Size = New System.Drawing.Size(311, 52)
        Me.su3.TabIndex = 42
        Me.su3.Text = "Inscrever Outra Turma"
        Me.su3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.su3.UseVisualStyleBackColor = False
        Me.su3.Visible = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.turmas
        Me.PictureBox3.Location = New System.Drawing.Point(11, 65)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 25
        Me.PictureBox3.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(11, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 21
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(987, 57)
        Me.PictureBox2.TabIndex = 22
        Me.PictureBox2.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DeepSkyBlue
        Me.PictureBox4.Location = New System.Drawing.Point(-2, 57)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 26
        Me.PictureBox4.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.WhiteSmoke
        Me.PictureBox7.Location = New System.Drawing.Point(184, 57)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(802, 97)
        Me.PictureBox7.TabIndex = 28
        Me.PictureBox7.TabStop = False
        '
        'inscreverTurma
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(986, 658)
        Me.Controls.Add(Me.s4)
        Me.Controls.Add(Me.su1)
        Me.Controls.Add(Me.su2)
        Me.Controls.Add(Me.su3)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox7)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "inscreverTurma"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "InscreverTurma"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.DossieProjectoDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.su1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents OvalShape1 As Microsoft.VisualBasic.PowerPacks.OvalShape
    Friend WithEvents letra As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents curso As System.Windows.Forms.TextBox
    Friend WithEvents ano As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents nAlunos As System.Windows.Forms.TextBox
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents TurmaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.TurmaTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents su2 As System.Windows.Forms.Label
    Friend WithEvents su1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents su3 As System.Windows.Forms.Button
    Friend WithEvents s4 As System.Windows.Forms.Button
    Friend WithEvents DossieProjectoDataSet1 As Profgest.DossieProjectoDataSet
    Friend WithEvents TurmaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TurmaTableAdapter1 As Profgest.DossieProjectoDataSetTableAdapters.TurmaTableAdapter
    Friend WithEvents TableAdapterManager1 As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
End Class
