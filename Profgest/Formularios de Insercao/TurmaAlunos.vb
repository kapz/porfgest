﻿Public Class TurmaAlunos

    Public selectedTurma As Integer = Nothing
    Private Sub Label4_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub letra_SelectedIndexChanged(sender As Object, e As EventArgs) Handles turma.SelectedIndexChanged
        Me.DossieProjectoDataSet.Aluno.Clear()
        Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, turma.SelectedValue)
    End Sub

    Private Sub AlunoBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.AlunoBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)

    End Sub

    Private Sub TurmaAlunos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.TurmaFormated' table. You can move, or remove it, as needed.
        Me.TurmaFormatedTableAdapter.Fill(Me.DossieProjectoDataSet.TurmaFormated)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Turma' table. You can move, or remove it, as needed.
        Me.TurmaTableAdapter.Fill(Me.DossieProjectoDataSet.Turma)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Aluno' table. You can move, or remove it, as needed.
        Me.AlunoTableAdapter.Fill(Me.DossieProjectoDataSet.Aluno)

        NumeroTextBox.Text = 1
        If (Not IsNothing(selectedTurma)) Then
            turma.SelectedValue = selectedTurma
            Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, selectedTurma)
        End If

    End Sub



    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Me.AlunoBindingSource.MoveNext()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.AlunoBindingSource.MovePrevious()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim numerov, nomev, emailv, telemovelv, telencarv
        Dim existeErro As Boolean = False
        Dim erros(6) As String
        Dim strErr As String = ""
        Dim erroi As Integer = 0

        numerov = CInt(NumeroTextBox.Text)
        nomev = NomeTextBox.Text
        emailv = EmailTextBox.Text
        telemovelv = TelefoneTextBox.Text
        telencarv = TelEncarregadoTextBox.Text

        If (existeErro) Then
            For i = 0 To erroi
                strErr &= "-> " & erros(i) & vbNewLine
            Next
            MessageBox.Show("Ocoreram os seguintes erros:" & vbNewLine & strErr, "Erros", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Else
            Try
                Me.AlunoTableAdapter.InsertAluno(turma.SelectedValue, nomev, numerov, emailv, telemovelv, telencarv)
                Me.DossieProjectoDataSet.Aluno.Clear()
                Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, selectedTurma)
            Catch ex As Exception
                MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        NumeroTextBox.Clear()
        NomeTextBox.Clear()
        TelefoneTextBox.Clear()
        TelEncarregadoTextBox.Clear()
        EmailTextBox.Clear()

    End Sub
End Class