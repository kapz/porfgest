﻿Public Class inscreverTurma
    Public lastId As Integer = Nothing


    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click

        Dim anov, cursov, letrav
        Dim existeErro As Boolean = False
        Dim erros(6) As String
        Dim strErr As String = ""
        Dim erroi As Integer = 0

        anov = ano.Text
        cursov = curso.Text
        letrav = letra.Text

        If (Len(letrav) < 0 Or Len(letrav) > 1) Then
            existeErro = True
            erros(erroi) = "Letra da turma é invalida."
            erroi += 1
        End If
        If (Not IsNumeric(anov)) Then
            existeErro = True
            erros(erroi) = "Ano não é numerico"
            erroi += 1
        Else
            anov = CInt(anov)
        End If

        If (cursov = "") Then
            existeErro = True
            erros(erroi) = "Curso não pode estar em branco"
        End If
        If (existeErro) Then
            For i = 0 To erroi
                strErr &= "-> " & erros(i) & vbNewLine
            Next
            MessageBox.Show("Ocoreram os seguintes erros:" & vbNewLine & strErr, "Erros", MessageBoxButtons.OK, MessageBoxIcon.Stop)
        Else
            Try
                Me.TurmaTableAdapter1.InsertTurma(letrav, anov, cursov)

                ' Obter Ultimo id Inserido'
                lastId = Me.TurmaTableAdapter1.lastID()
                Panel1.Visible = False
                su1.Visible = True
                su2.Visible = True
                su3.Visible = True
                s4.Visible = True
                Me.TurmaTableAdapter1.Fill(Me.DossieProjectoDataSet1.Turma)
                Turmas.TurmaTableAdapter.Fill(Turmas.DossieProjectoDataSet.Turma)

            Catch ex As Exception
                MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub


    Private Sub inscreverTurma_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet1.Turma' table. You can move, or remove it, as needed.
        Me.TurmaTableAdapter1.Fill(Me.DossieProjectoDataSet1.Turma)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet1.Turma' table. You can move, or remove it, as needed.
        Me.TurmaTableAdapter1.Fill(Me.DossieProjectoDataSet1.Turma)

    End Sub

    Private Sub su3_Click(sender As Object, e As EventArgs) Handles su3.Click
        Panel1.Visible = True
        ano.Text = ""
        letra.Text = ""
        curso.Text = ""
        nAlunos.Text = ""
        su1.Visible = False
        su2.Visible = False
        su3.Visible = False
        s4.Visible = False
    End Sub

    Private Sub TurmaBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.TurmaBindingSource.EndEdit()
        Me.TableAdapterManager1.UpdateAll(Me.DossieProjectoDataSet1)

    End Sub

    Private Sub s4_Click(sender As Object, e As EventArgs) Handles s4.Click
        TurmaAlunos.selectedTurma = lastId
        TurmaAlunos.ShowDialog()

        Me.Hide()
    End Sub
End Class