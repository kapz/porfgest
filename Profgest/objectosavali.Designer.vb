﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class objectosavali
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NomeLabel As System.Windows.Forms.Label
        Dim PercentagemLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.AvaliacaoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DossieProjectoDataSet = New Profgest.DossieProjectoDataSet()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.AvaliacaoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AvaliacaoTableAdapter()
        Me.ObjectoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObjectoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.ObjectoTableAdapter()
        Me.TableAdapterManager = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.ObjectoDataGridView = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.percentagem = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.X = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.AreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.AreaCombo = New System.Windows.Forms.ComboBox()
        Me.AvaliacaoAvaliAreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliAreaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AvaliAreaTableAdapter()
        Me.AreaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter()
        Me.AreaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        NomeLabel = New System.Windows.Forms.Label()
        PercentagemLabel = New System.Windows.Forms.Label()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ObjectoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ObjectoDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.AvaliacaoAvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AreaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NomeLabel
        '
        NomeLabel.AutoSize = True
        NomeLabel.Location = New System.Drawing.Point(8, 113)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(38, 13)
        NomeLabel.TabIndex = 111
        NomeLabel.Text = "Nome:"
        '
        'PercentagemLabel
        '
        PercentagemLabel.AutoSize = True
        PercentagemLabel.Location = New System.Drawing.Point(8, 140)
        PercentagemLabel.Name = "PercentagemLabel"
        PercentagemLabel.Size = New System.Drawing.Size(44, 13)
        PercentagemLabel.TabIndex = 112
        PercentagemLabel.Text = "Percen."
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox5.Image = Global.Profgest.My.Resources.Resources.help
        Me.PictureBox5.Location = New System.Drawing.Point(835, 7)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(67, 43)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 104
        Me.PictureBox5.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.a4f1f8f2_63f5_436d_b0d8_f325b11a22fc
        Me.PictureBox3.Location = New System.Drawing.Point(16, 60)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 102
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox4.Location = New System.Drawing.Point(2, 54)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 103
        Me.PictureBox4.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(11, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 99
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(72, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 33)
        Me.Label1.TabIndex = 98
        Me.Label1.Text = "Profgest"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(-1, -3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(1012, 57)
        Me.PictureBox2.TabIndex = 100
        Me.PictureBox2.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        Me.PictureBox7.Location = New System.Drawing.Point(182, 49)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(829, 102)
        Me.PictureBox7.TabIndex = 101
        Me.PictureBox7.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(194, 72)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(496, 55)
        Me.Label2.TabIndex = 105
        Me.Label2.Text = "Objectos de avaliação"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.AvaliacaoBindingSource
        Me.ComboBox1.DisplayMember = "codAvaliacao"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(78, 177)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(190, 21)
        Me.ComboBox1.TabIndex = 106
        Me.ComboBox1.ValueMember = "codAvaliacao"
        '
        'AvaliacaoBindingSource
        '
        Me.AvaliacaoBindingSource.DataMember = "Avaliacao"
        Me.AvaliacaoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'DossieProjectoDataSet
        '
        Me.DossieProjectoDataSet.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 181)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 107
        Me.Label3.Text = "Avaliação"
        '
        'AvaliacaoTableAdapter
        '
        Me.AvaliacaoTableAdapter.ClearBeforeFill = True
        '
        'ObjectoBindingSource
        '
        Me.ObjectoBindingSource.DataMember = "Objecto"
        Me.ObjectoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ObjectoTableAdapter
        '
        Me.ObjectoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager.AlunoTableAdapter = Nothing
        Me.TableAdapterManager.AreaTableAdapter = Nothing
        Me.TableAdapterManager.AvaliacaoTableAdapter = Me.AvaliacaoTableAdapter
        Me.TableAdapterManager.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DisciplinaTableAdapter = Nothing
        Me.TableAdapterManager.HelpdeskTableAdapter = Nothing
        Me.TableAdapterManager.ModuloTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoTableAdapter = Me.ObjectoTableAdapter
        Me.TableAdapterManager.SumarioTableAdapter = Nothing
        Me.TableAdapterManager.TurmaFormatedTableAdapter = Nothing
        Me.TableAdapterManager.TurmaTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ObjectoDataGridView
        '
        Me.ObjectoDataGridView.AllowUserToAddRows = False
        Me.ObjectoDataGridView.AllowUserToDeleteRows = False
        Me.ObjectoDataGridView.AutoGenerateColumns = False
        Me.ObjectoDataGridView.BackgroundColor = System.Drawing.SystemColors.Control
        Me.ObjectoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ObjectoDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.DataGridViewTextBoxColumn4, Me.percentagem, Me.X})
        Me.ObjectoDataGridView.DataSource = Me.ObjectoBindingSource
        Me.ObjectoDataGridView.Location = New System.Drawing.Point(167, 60)
        Me.ObjectoDataGridView.Name = "ObjectoDataGridView"
        Me.ObjectoDataGridView.Size = New System.Drawing.Size(443, 225)
        Me.ObjectoDataGridView.TabIndex = 107
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "codObj"
        Me.Column1.HeaderText = "Column1"
        Me.Column1.Name = "Column1"
        Me.Column1.Visible = False
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "nome"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Nome"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 200
        '
        'percentagem
        '
        Me.percentagem.DataPropertyName = "percentagem"
        Me.percentagem.HeaderText = "percentagem"
        Me.percentagem.Name = "percentagem"
        '
        'X
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.IndianRed
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        Me.X.DefaultCellStyle = DataGridViewCellStyle1
        Me.X.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.X.HeaderText = ""
        Me.X.Name = "X"
        Me.X.Text = "X"
        Me.X.UseColumnTextForButtonValue = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Controls.Add(Me.ObjectoDataGridView)
        Me.GroupBox1.Location = New System.Drawing.Point(274, 173)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(627, 302)
        Me.GroupBox1.TabIndex = 108
        Me.GroupBox1.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(18, 38)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 20)
        Me.Label6.TabIndex = 109
        Me.Label6.Text = "Area"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(479, 24)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(131, 33)
        Me.Label5.TabIndex = 72
        Me.Label5.Text = "Objectos"
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.AreaBindingSource
        Me.ListBox1.DisplayMember = "nome"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(16, 60)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(145, 225)
        Me.ListBox1.TabIndex = 108
        Me.ListBox1.ValueMember = "codArea"
        '
        'AreaBindingSource
        '
        Me.AreaBindingSource.DataMember = "Area"
        Me.AreaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(PercentagemLabel)
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(NomeLabel)
        Me.GroupBox2.Controls.Add(Me.NomeTextBox)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.AreaCombo)
        Me.GroupBox2.Location = New System.Drawing.Point(15, 204)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(253, 271)
        Me.GroupBox2.TabIndex = 109
        Me.GroupBox2.TabStop = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(147, 139)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(15, 13)
        Me.Label8.TabIndex = 110
        Me.Label8.Text = "%"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(6, 204)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(241, 50)
        Me.Button2.TabIndex = 110
        Me.Button2.Text = "Adicionar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = False
        '
        'NomeTextBox
        '
        Me.NomeTextBox.Location = New System.Drawing.Point(66, 110)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(181, 20)
        Me.NomeTextBox.TabIndex = 112
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 86)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(29, 13)
        Me.Label7.TabIndex = 111
        Me.Label7.Text = "Area"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(6, 32)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(185, 33)
        Me.Label4.TabIndex = 71
        Me.Label4.Text = "Novo objecto"
        '
        'AreaCombo
        '
        Me.AreaCombo.DataSource = Me.AreaBindingSource
        Me.AreaCombo.DisplayMember = "nome"
        Me.AreaCombo.FormattingEnabled = True
        Me.AreaCombo.Location = New System.Drawing.Point(66, 83)
        Me.AreaCombo.Name = "AreaCombo"
        Me.AreaCombo.Size = New System.Drawing.Size(181, 21)
        Me.AreaCombo.TabIndex = 110
        Me.AreaCombo.ValueMember = "codArea"
        '
        'AvaliacaoAvaliAreaBindingSource
        '
        Me.AvaliacaoAvaliAreaBindingSource.DataMember = "AvaliacaoAvaliArea"
        Me.AvaliacaoAvaliAreaBindingSource.DataSource = Me.AvaliacaoBindingSource
        '
        'AvaliAreaTableAdapter
        '
        Me.AvaliAreaTableAdapter.ClearBeforeFill = True
        '
        'AreaTableAdapter
        '
        Me.AreaTableAdapter.ClearBeforeFill = True
        '
        'AreaBindingSource1
        '
        Me.AreaBindingSource1.DataMember = "Area"
        Me.AreaBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.DecimalPlaces = 2
        Me.NumericUpDown1.Location = New System.Drawing.Point(66, 136)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(75, 20)
        Me.NumericUpDown1.TabIndex = 113
        '
        'objectosavali
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(914, 492)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox7)
        Me.Name = "objectosavali"
        Me.Text = "Objectos de avaliação"
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ObjectoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ObjectoDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.AvaliacaoAvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AreaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents AvaliacaoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliacaoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AvaliacaoTableAdapter
    Friend WithEvents ObjectoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ObjectoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.ObjectoTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ObjectoDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents AvaliacaoAvaliAreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliAreaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AvaliAreaTableAdapter
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents AreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AreaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents AreaCombo As System.Windows.Forms.ComboBox
    Friend WithEvents AreaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents percentagem As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents X As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
End Class
