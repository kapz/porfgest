﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class areasteste
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodAreaLabel As System.Windows.Forms.Label
        Dim NomeLabel As System.Windows.Forms.Label
        Dim PercentagemLabel As System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DossieProjectoDataSet = New Profgest.DossieProjectoDataSet()
        Me.AreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AreaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter()
        Me.TableAdapterManager = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.CodAreaTextBox = New System.Windows.Forms.TextBox()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.PercentagemTextBox = New System.Windows.Forms.TextBox()
        CodAreaLabel = New System.Windows.Forms.Label()
        NomeLabel = New System.Windows.Forms.Label()
        PercentagemLabel = New System.Windows.Forms.Label()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CodAreaLabel
        '
        CodAreaLabel.AutoSize = True
        CodAreaLabel.Location = New System.Drawing.Point(126, 270)
        CodAreaLabel.Name = "CodAreaLabel"
        CodAreaLabel.Size = New System.Drawing.Size(53, 13)
        CodAreaLabel.TabIndex = 80
        CodAreaLabel.Text = "cod Area:"
        '
        'NomeLabel
        '
        NomeLabel.AutoSize = True
        NomeLabel.Location = New System.Drawing.Point(143, 300)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(36, 13)
        NomeLabel.TabIndex = 81
        NomeLabel.Text = "nome:"
        '
        'PercentagemLabel
        '
        PercentagemLabel.AutoSize = True
        PercentagemLabel.Location = New System.Drawing.Point(107, 326)
        PercentagemLabel.Name = "PercentagemLabel"
        PercentagemLabel.Size = New System.Drawing.Size(72, 13)
        PercentagemLabel.TabIndex = 82
        PercentagemLabel.Text = "percentagem:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(203, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 65)
        Me.Label2.TabIndex = 36
        Me.Label2.Text = "Areas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Open Sans Light", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(70, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 39)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Profgest"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.a4f1f8f2_63f5_436d_b0d8_f325b11a22fc
        Me.PictureBox3.Location = New System.Drawing.Point(14, 63)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 34
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox4.Location = New System.Drawing.Point(1, 55)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 35
        Me.PictureBox4.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(9, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 30
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(-3, -1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(546, 57)
        Me.PictureBox2.TabIndex = 31
        Me.PictureBox2.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        Me.PictureBox7.Location = New System.Drawing.Point(181, 55)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(364, 97)
        Me.PictureBox7.TabIndex = 33
        Me.PictureBox7.TabStop = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.LightSalmon
        Me.Button9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button9.Location = New System.Drawing.Point(110, 385)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(81, 37)
        Me.Button9.TabIndex = 78
        Me.Button9.Text = "Novo"
        Me.Button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.LightCoral
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(195, 385)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(30, 37)
        Me.Button5.TabIndex = 74
        Me.Button5.Text = "X"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.Location = New System.Drawing.Point(394, 310)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(30, 36)
        Me.Button4.TabIndex = 73
        Me.Button4.Text = ">"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(394, 266)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(30, 33)
        Me.Button3.TabIndex = 72
        Me.Button3.Text = "<"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Open Sans", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(103, 203)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 39)
        Me.Label3.TabIndex = 69
        Me.Label3.Text = "Areas"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Enabled = False
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(232, 385)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(192, 37)
        Me.Button1.TabIndex = 68
        Me.Button1.Text = "Adicionar"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = False
        '
        'DossieProjectoDataSet
        '
        Me.DossieProjectoDataSet.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AreaBindingSource
        '
        Me.AreaBindingSource.DataMember = "Area"
        Me.AreaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'AreaTableAdapter
        '
        Me.AreaTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager.AlunoTableAdapter = Nothing
        Me.TableAdapterManager.AreaTableAdapter = Me.AreaTableAdapter
        Me.TableAdapterManager.AvaliacaoTableAdapter = Nothing
        Me.TableAdapterManager.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DisciplinaTableAdapter = Nothing
        Me.TableAdapterManager.ModuloTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoTableAdapter = Nothing
        Me.TableAdapterManager.SumarioTableAdapter = Nothing
        Me.TableAdapterManager.TurmaFormatedTableAdapter = Nothing
        Me.TableAdapterManager.TurmaTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'CodAreaTextBox
        '
        Me.CodAreaTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AreaBindingSource, "codArea", True))
        Me.CodAreaTextBox.Enabled = False
        Me.CodAreaTextBox.Location = New System.Drawing.Point(185, 267)
        Me.CodAreaTextBox.Name = "CodAreaTextBox"
        Me.CodAreaTextBox.Size = New System.Drawing.Size(55, 20)
        Me.CodAreaTextBox.TabIndex = 81
        '
        'NomeTextBox
        '
        Me.NomeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AreaBindingSource, "nome", True))
        Me.NomeTextBox.Location = New System.Drawing.Point(185, 297)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(190, 20)
        Me.NomeTextBox.TabIndex = 82
        '
        'PercentagemTextBox
        '
        Me.PercentagemTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.AreaBindingSource, "percentagem", True))
        Me.PercentagemTextBox.Location = New System.Drawing.Point(185, 323)
        Me.PercentagemTextBox.Name = "PercentagemTextBox"
        Me.PercentagemTextBox.Size = New System.Drawing.Size(55, 20)
        Me.PercentagemTextBox.TabIndex = 83
        '
        'areasteste
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(543, 528)
        Me.Controls.Add(PercentagemLabel)
        Me.Controls.Add(Me.PercentagemTextBox)
        Me.Controls.Add(NomeLabel)
        Me.Controls.Add(Me.NomeTextBox)
        Me.Controls.Add(CodAreaLabel)
        Me.Controls.Add(Me.CodAreaTextBox)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox7)
        Me.Name = "areasteste"
        Me.Text = "areasteste"
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents AreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AreaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents CodAreaTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents PercentagemTextBox As System.Windows.Forms.TextBox
End Class
