﻿Public Class Notas

 

    Private Sub Notas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Aluno' table. You can move, or remove it, as needed.
        Me.AlunoTableAdapter.Fill(Me.DossieProjectoDataSet.Aluno)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.ObjectoNotaAluno' table. You can move, or remove it, as needed.
        Me.ObjectoNotaAlunoTableAdapter.Fill(Me.DossieProjectoDataSet.ObjectoNotaAluno)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.NotasObjecto' table. You can move, or remove it, as needed.
        Me.NotasObjectoTableAdapter.Fill(Me.DossieProjectoDataSet.NotasObjecto)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Avaliacao' table. You can move, or remove it, as needed.
        Me.AvaliacaoTableAdapter.Fill(Me.DossieProjectoDataSet.Avaliacao)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Area' table. You can move, or remove it, as needed.
        Me.AreaTableAdapter.Fill(Me.DossieProjectoDataSet.Area)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Objecto' table. You can move, or remove it, as needed.
        Me.ObjectoTableAdapter.Fill(Me.DossieProjectoDataSet.Objecto)


        If (avaliacao.codavaliacao_escolhida <> 0) Then
            ComboBox1.SelectedValue = avaliacao.codavaliacao_escolhida
            Dim turma = Me.AvaliacaoTableAdapter.ScalarQuery(ComboBox1.SelectedValue)
            Me.AreaTableAdapter.FillBy1(Me.DossieProjectoDataSet.Area, ComboBox1.SelectedValue)
            Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, turma)
            Me.ObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue)
            Me.NotasObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.NotasObjecto, ListBox1.SelectedValue)
        End If
    End Sub



    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.ObjectoTableAdapter.FillBy(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue, ComboBox2.SelectedValue)
        Me.NotasObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.NotasObjecto, ListBox1.SelectedValue)
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim turma = Me.AvaliacaoTableAdapter.ScalarQuery(ComboBox1.SelectedValue)
        Me.AreaTableAdapter.FillBy1(Me.DossieProjectoDataSet.Area, ComboBox1.SelectedValue)
        Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, turma)
        Me.ObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue)
        Me.NotasObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.NotasObjecto, ListBox1.SelectedValue)
    End Sub

    Private Sub Label7_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub GroupBox1_Enter(sender As Object, e As EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub Label6_Click(sender As Object, e As EventArgs) Handles Label6.Click

    End Sub

    Private Sub Notas_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If (avaliacao.codavaliacao_escolhida <> 0) Then
            ComboBox1.SelectedValue = avaliacao.codavaliacao_escolhida
            Dim turma = Me.AvaliacaoTableAdapter.ScalarQuery(ComboBox1.SelectedValue)
            Me.AreaTableAdapter.FillBy1(Me.DossieProjectoDataSet.Area, ComboBox1.SelectedValue)
            Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, turma)
            Me.ObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.Objecto, ComboBox1.SelectedValue)
            Me.NotasObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.NotasObjecto, ListBox1.SelectedValue)
        End If
    End Sub

    Private Sub GroupBox3_Enter(sender As Object, e As EventArgs) Handles GroupBox3.Enter

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Me.ObjectoNotaAlunoTableAdapter.InsertQuery(ListBox1.SelectedValue, NomeComboBox.SelectedValue, NumericUpDown1.Value)
            Me.NotasObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.NotasObjecto, ListBox1.SelectedValue)
            MessageBox.Show("Nota adicionada!", "Sucesso", MessageBoxButtons.OK)
            NomeComboBox.Text = ""
            NumericUpDown1.Value = 0

        Catch ex As Exception
            MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NotasObjectoDataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles NotasObjectoDataGridView.CellContentClick
        If e.ColumnIndex = 5 Then

            Dim id As Integer = NotasObjectoDataGridView.Rows(e.RowIndex).Cells(0).Value
            Dim id2 As Integer = NotasObjectoDataGridView.Rows(e.RowIndex).Cells(1).Value
            Dim result = MessageBox.Show("Deseja mesmo eleminar esta nota?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If (result = DialogResult.Yes) Then
                Try

                    Me.ObjectoNotaAlunoTableAdapter.DeleteQuery(id, id2)
                    Me.NotasObjectoTableAdapter.FillBy(Me.DossieProjectoDataSet.NotasObjecto, ComboBox2.SelectedValue, ComboBox1.SelectedValue)
                    Me.Validate()
                    Me.AvaliacaoBindingSource.EndEdit()
                Catch ex As Exception
                    MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        End If
    End Sub

    Private Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBox1.SelectedIndexChanged
        Me.NotasObjectoTableAdapter.FillBy1(Me.DossieProjectoDataSet.NotasObjecto, ListBox1.SelectedValue)
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        help.Show()
    End Sub
End Class