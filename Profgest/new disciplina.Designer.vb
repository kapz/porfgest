﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class new_disciplina
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodDiscipLabel As System.Windows.Forms.Label
        Dim CodTurmaLabel As System.Windows.Forms.Label
        Dim NomeLabel1 As System.Windows.Forms.Label
        Dim ObjectivosLabel As System.Windows.Forms.Label
        Dim HorasLabel As System.Windows.Forms.Label
        Dim NumeroLabel As System.Windows.Forms.Label
        Dim NomeLabel2 As System.Windows.Forms.Label
        Dim CodDiscipLabel2 As System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.DossieProjectoDataSet = New Profgest.DossieProjectoDataSet()
        Me.DisciplinaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DisciplinaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.DisciplinaTableAdapter()
        Me.TableAdapterManager = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.ModuloTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.ModuloTableAdapter()
        Me.TurmaFormatedTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter()
        Me.TurmaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.TurmaTableAdapter()
        Me.TurmaFormatedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TurmaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModuloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.CodDiscipTextBox = New System.Windows.Forms.TextBox()
        Me.NomeTextBox = New System.Windows.Forms.TextBox()
        Me.ObjectivosTextBox = New System.Windows.Forms.TextBox()
        Me.HorasTextBox = New System.Windows.Forms.TextBox()
        Me.NumeroTextBox = New System.Windows.Forms.TextBox()
        Me.NomeTextBox1 = New System.Windows.Forms.TextBox()
        Me.TurmaFormatedBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.TurmaDisciplina1BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DisciplinaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        CodDiscipLabel = New System.Windows.Forms.Label()
        CodTurmaLabel = New System.Windows.Forms.Label()
        NomeLabel1 = New System.Windows.Forms.Label()
        ObjectivosLabel = New System.Windows.Forms.Label()
        HorasLabel = New System.Windows.Forms.Label()
        NumeroLabel = New System.Windows.Forms.Label()
        NomeLabel2 = New System.Windows.Forms.Label()
        CodDiscipLabel2 = New System.Windows.Forms.Label()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DisciplinaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModuloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaDisciplina1BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DisciplinaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CodDiscipLabel
        '
        CodDiscipLabel.AutoSize = True
        CodDiscipLabel.Location = New System.Drawing.Point(48, 254)
        CodDiscipLabel.Name = "CodDiscipLabel"
        CodDiscipLabel.Size = New System.Drawing.Size(43, 13)
        CodDiscipLabel.TabIndex = 67
        CodDiscipLabel.Text = " Codigo"
        '
        'CodTurmaLabel
        '
        CodTurmaLabel.AutoSize = True
        CodTurmaLabel.Location = New System.Drawing.Point(51, 302)
        CodTurmaLabel.Name = "CodTurmaLabel"
        CodTurmaLabel.Size = New System.Drawing.Size(40, 13)
        CodTurmaLabel.TabIndex = 68
        CodTurmaLabel.Text = "Turma:"
        '
        'NomeLabel1
        '
        NomeLabel1.AutoSize = True
        NomeLabel1.Location = New System.Drawing.Point(497, 313)
        NomeLabel1.Name = "NomeLabel1"
        NomeLabel1.Size = New System.Drawing.Size(36, 13)
        NomeLabel1.TabIndex = 72
        NomeLabel1.Text = "nome:"
        '
        'ObjectivosLabel
        '
        ObjectivosLabel.AutoSize = True
        ObjectivosLabel.Location = New System.Drawing.Point(497, 343)
        ObjectivosLabel.Name = "ObjectivosLabel"
        ObjectivosLabel.Size = New System.Drawing.Size(58, 13)
        ObjectivosLabel.TabIndex = 73
        ObjectivosLabel.Text = "objectivos:"
        '
        'HorasLabel
        '
        HorasLabel.AutoSize = True
        HorasLabel.Location = New System.Drawing.Point(682, 279)
        HorasLabel.Name = "HorasLabel"
        HorasLabel.Size = New System.Drawing.Size(38, 13)
        HorasLabel.TabIndex = 74
        HorasLabel.Text = "Horas:"
        '
        'NumeroLabel
        '
        NumeroLabel.AutoSize = True
        NumeroLabel.Location = New System.Drawing.Point(676, 250)
        NumeroLabel.Name = "NumeroLabel"
        NumeroLabel.Size = New System.Drawing.Size(44, 13)
        NumeroLabel.TabIndex = 77
        NumeroLabel.Text = "Numero"
        '
        'NomeLabel2
        '
        NomeLabel2.AutoSize = True
        NomeLabel2.Location = New System.Drawing.Point(55, 343)
        NomeLabel2.Name = "NomeLabel2"
        NomeLabel2.Size = New System.Drawing.Size(36, 13)
        NomeLabel2.TabIndex = 78
        NomeLabel2.Text = "nome:"
        '
        'CodDiscipLabel2
        '
        CodDiscipLabel2.AutoSize = True
        CodDiscipLabel2.Location = New System.Drawing.Point(495, 263)
        CodDiscipLabel2.Name = "CodDiscipLabel2"
        CodDiscipLabel2.Size = New System.Drawing.Size(52, 13)
        CodDiscipLabel2.TabIndex = 82
        CodDiscipLabel2.Text = "Disciplina"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(205, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(492, 65)
        Me.Label2.TabIndex = 43
        Me.Label2.Text = "Disciplinas e Modulos"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Open Sans Light", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(72, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 39)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Profgest"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.a4f1f8f2_63f5_436d_b0d8_f325b11a22fc
        Me.PictureBox3.Location = New System.Drawing.Point(16, 63)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 41
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox4.Location = New System.Drawing.Point(3, 55)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 42
        Me.PictureBox4.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        Me.PictureBox7.Location = New System.Drawing.Point(185, 55)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(706, 97)
        Me.PictureBox7.TabIndex = 40
        Me.PictureBox7.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(11, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 37
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(-1, -1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(892, 57)
        Me.PictureBox2.TabIndex = 38
        Me.PictureBox2.TabStop = False
        '
        'DossieProjectoDataSet
        '
        Me.DossieProjectoDataSet.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DisciplinaBindingSource
        '
        Me.DisciplinaBindingSource.DataMember = "Disciplina"
        Me.DisciplinaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'DisciplinaTableAdapter
        '
        Me.DisciplinaTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager.AlunoTableAdapter = Nothing
        Me.TableAdapterManager.AreaTableAdapter = Nothing
        Me.TableAdapterManager.AvaliacaoTableAdapter = Nothing
        Me.TableAdapterManager.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DisciplinaTableAdapter = Me.DisciplinaTableAdapter
        Me.TableAdapterManager.ModuloTableAdapter = Me.ModuloTableAdapter
        Me.TableAdapterManager.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoTableAdapter = Nothing
        Me.TableAdapterManager.SumarioTableAdapter = Nothing
        Me.TableAdapterManager.TurmaFormatedTableAdapter = Me.TurmaFormatedTableAdapter
        Me.TableAdapterManager.TurmaTableAdapter = Me.TurmaTableAdapter
        Me.TableAdapterManager.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ModuloTableAdapter
        '
        Me.ModuloTableAdapter.ClearBeforeFill = True
        '
        'TurmaFormatedTableAdapter
        '
        Me.TurmaFormatedTableAdapter.ClearBeforeFill = True
        '
        'TurmaTableAdapter
        '
        Me.TurmaTableAdapter.ClearBeforeFill = True
        '
        'TurmaFormatedBindingSource
        '
        Me.TurmaFormatedBindingSource.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.Enabled = False
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(163, 396)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(192, 37)
        Me.Button1.TabIndex = 49
        Me.Button1.Text = "Adicionar"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TurmaBindingSource
        '
        Me.TurmaBindingSource.DataMember = "Turma"
        Me.TurmaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ModuloBindingSource
        '
        Me.ModuloBindingSource.DataMember = "Modulo"
        Me.ModuloBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Open Sans", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(51, 184)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(160, 39)
        Me.Label3.TabIndex = 56
        Me.Label3.Text = "Disciplinas"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Open Sans", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(491, 184)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(136, 39)
        Me.Label4.TabIndex = 57
        Me.Label4.Text = "Modulos"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(604, 396)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(233, 37)
        Me.Button2.TabIndex = 59
        Me.Button2.Text = "Adicionar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(325, 247)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(30, 33)
        Me.Button3.TabIndex = 60
        Me.Button3.Text = "<"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.Location = New System.Drawing.Point(325, 291)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(30, 36)
        Me.Button4.TabIndex = 61
        Me.Button4.Text = ">"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.LightCoral
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(126, 396)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(30, 37)
        Me.Button5.TabIndex = 62
        Me.Button5.Text = "X"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.LightCoral
        Me.Button6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.Location = New System.Drawing.Point(561, 396)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(30, 37)
        Me.Button6.TabIndex = 65
        Me.Button6.Text = "X"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button7.ForeColor = System.Drawing.Color.White
        Me.Button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button7.Location = New System.Drawing.Point(807, 291)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(30, 32)
        Me.Button7.TabIndex = 64
        Me.Button7.Text = ">"
        Me.Button7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button8.ForeColor = System.Drawing.Color.White
        Me.Button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button8.Location = New System.Drawing.Point(807, 248)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(30, 32)
        Me.Button8.TabIndex = 63
        Me.Button8.Text = "<"
        Me.Button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.LightSalmon
        Me.Button9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button9.ForeColor = System.Drawing.Color.White
        Me.Button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button9.Location = New System.Drawing.Point(54, 396)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(68, 37)
        Me.Button9.TabIndex = 66
        Me.Button9.Text = "Novo"
        Me.Button9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.DarkSalmon
        Me.Button10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button10.ForeColor = System.Drawing.Color.White
        Me.Button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button10.Location = New System.Drawing.Point(491, 396)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(68, 37)
        Me.Button10.TabIndex = 67
        Me.Button10.Text = "Novo"
        Me.Button10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button10.UseVisualStyleBackColor = False
        '
        'CodDiscipTextBox
        '
        Me.CodDiscipTextBox.CausesValidation = False
        Me.CodDiscipTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DisciplinaBindingSource, "codDiscip", True))
        Me.CodDiscipTextBox.Enabled = False
        Me.CodDiscipTextBox.Location = New System.Drawing.Point(97, 251)
        Me.CodDiscipTextBox.Name = "CodDiscipTextBox"
        Me.CodDiscipTextBox.Size = New System.Drawing.Size(72, 20)
        Me.CodDiscipTextBox.TabIndex = 68
        '
        'NomeTextBox
        '
        Me.NomeTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ModuloBindingSource, "nome", True))
        Me.NomeTextBox.Location = New System.Drawing.Point(561, 307)
        Me.NomeTextBox.Name = "NomeTextBox"
        Me.NomeTextBox.Size = New System.Drawing.Size(204, 20)
        Me.NomeTextBox.TabIndex = 73
        '
        'ObjectivosTextBox
        '
        Me.ObjectivosTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ModuloBindingSource, "objectivos", True))
        Me.ObjectivosTextBox.Location = New System.Drawing.Point(561, 343)
        Me.ObjectivosTextBox.Multiline = True
        Me.ObjectivosTextBox.Name = "ObjectivosTextBox"
        Me.ObjectivosTextBox.Size = New System.Drawing.Size(276, 47)
        Me.ObjectivosTextBox.TabIndex = 74
        '
        'HorasTextBox
        '
        Me.HorasTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ModuloBindingSource, "horas", True))
        Me.HorasTextBox.Location = New System.Drawing.Point(732, 276)
        Me.HorasTextBox.Name = "HorasTextBox"
        Me.HorasTextBox.Size = New System.Drawing.Size(44, 20)
        Me.HorasTextBox.TabIndex = 75
        '
        'NumeroTextBox
        '
        Me.NumeroTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ModuloBindingSource, "numero", True))
        Me.NumeroTextBox.Location = New System.Drawing.Point(732, 247)
        Me.NumeroTextBox.Name = "NumeroTextBox"
        Me.NumeroTextBox.Size = New System.Drawing.Size(44, 20)
        Me.NumeroTextBox.TabIndex = 78
        '
        'NomeTextBox1
        '
        Me.NomeTextBox1.CausesValidation = False
        Me.NomeTextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DisciplinaBindingSource, "nome", True))
        Me.NomeTextBox1.Location = New System.Drawing.Point(97, 340)
        Me.NomeTextBox1.Multiline = True
        Me.NomeTextBox1.Name = "NomeTextBox1"
        Me.NomeTextBox1.Size = New System.Drawing.Size(258, 25)
        Me.NomeTextBox1.TabIndex = 79
        '
        'TurmaFormatedBindingSource1
        '
        Me.TurmaFormatedBindingSource1.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'ComboBox1
        '
        Me.ComboBox1.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.DisciplinaBindingSource, "codTurma", True))
        Me.ComboBox1.DataSource = Me.TurmaFormatedBindingSource
        Me.ComboBox1.DisplayMember = "TurmaData"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(97, 297)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(186, 21)
        Me.ComboBox1.TabIndex = 82
        Me.ComboBox1.ValueMember = "codTurma"
        '
        'TurmaDisciplina1BindingSource
        '
        Me.TurmaDisciplina1BindingSource.DataMember = "TurmaDisciplina1"
        Me.TurmaDisciplina1BindingSource.DataSource = Me.TurmaFormatedBindingSource
        '
        'DisciplinaBindingSource1
        '
        Me.DisciplinaBindingSource1.DataMember = "Disciplina"
        Me.DisciplinaBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.DisciplinaBindingSource
        Me.ComboBox2.DisplayMember = "nome"
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(561, 259)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(94, 21)
        Me.ComboBox2.TabIndex = 83
        Me.ComboBox2.ValueMember = "codDiscip"
        '
        'new_disciplina
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(891, 463)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(CodDiscipLabel2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(NomeLabel2)
        Me.Controls.Add(Me.NomeTextBox1)
        Me.Controls.Add(NumeroLabel)
        Me.Controls.Add(Me.NumeroTextBox)
        Me.Controls.Add(HorasLabel)
        Me.Controls.Add(Me.HorasTextBox)
        Me.Controls.Add(ObjectivosLabel)
        Me.Controls.Add(Me.ObjectivosTextBox)
        Me.Controls.Add(NomeLabel1)
        Me.Controls.Add(Me.NomeTextBox)
        Me.Controls.Add(CodTurmaLabel)
        Me.Controls.Add(CodDiscipLabel)
        Me.Controls.Add(Me.CodDiscipTextBox)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox7)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Name = "new_disciplina"
        Me.Text = "new_disciplina"
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DisciplinaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModuloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaDisciplina1BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DisciplinaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents DisciplinaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DisciplinaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.DisciplinaTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents TurmaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.TurmaTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TurmaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TurmaFormatedTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter
    Friend WithEvents TurmaFormatedBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModuloTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.ModuloTableAdapter
    Friend WithEvents ModuloBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents CodDiscipTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NomeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ObjectivosTextBox As System.Windows.Forms.TextBox
    Friend WithEvents HorasTextBox As System.Windows.Forms.TextBox
    Friend WithEvents NumeroTextBox As System.Windows.Forms.TextBox
    Friend WithEvents TurmaFormatedBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents NomeTextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents TurmaDisciplina1BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DisciplinaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
End Class
