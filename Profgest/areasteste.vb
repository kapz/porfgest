﻿Public Class areasteste

    Private Sub AreaBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.AreaBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)

    End Sub

    Private Sub areasteste_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Area' table. You can move, or remove it, as needed.
        Me.AreaTableAdapter.Fill(Me.DossieProjectoDataSet.Area)

    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        CodAreaTextBox.Clear()
        NomeTextBox.Clear()
        PercentagemTextBox.Clear()
        Button1.Enabled = True
        Button5.Enabled = False
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Try
            Me.AreaTableAdapter.DeleteQuery(CodAreaTextBox.Text)
            Me.AreaTableAdapter.Fill(Me.DossieProjectoDataSet.Area)
            Me.AreaBindingSource.MoveLast()

        Catch ex As Exception
            MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Me.AreaTableAdapter.InsertQuery(NomeTextBox.Text, PercentagemTextBox.Text)
            Me.AreaTableAdapter.Fill(Me.DossieProjectoDataSet.Area)
            Me.AreaBindingSource.MoveLast()
            Button1.Enabled = False
            Button5.Enabled = True
        Catch ex As Exception
            MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.AreaBindingSource.MovePrevious()
        Button1.Enabled = False
        Button5.Enabled = True
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.AreaBindingSource.MoveNext()
        Button1.Enabled = False
        Button5.Enabled = True
    End Sub
End Class