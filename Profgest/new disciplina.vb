﻿Public Class new_disciplina

    Private Sub DisciplinaBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.DisciplinaBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)

    End Sub


    Private Sub DisciplinaBindingNavigatorSaveItem_Click_1(sender As Object, e As EventArgs)
        Me.Validate()
        Me.DisciplinaBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)

    End Sub

    Private Sub new_disciplina_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Modulo' table. You can move, or remove it, as needed.
        Me.ModuloTableAdapter.Fill(Me.DossieProjectoDataSet.Modulo)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.TurmaFormated' table. You can move, or remove it, as needed.
        Me.TurmaFormatedTableAdapter.Fill(Me.DossieProjectoDataSet.TurmaFormated)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Turma' table. You can move, or remove it, as needed.
        Me.TurmaTableAdapter.Fill(Me.DossieProjectoDataSet.Turma)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Disciplina' table. You can move, or remove it, as needed.
        Me.DisciplinaTableAdapter.Fill(Me.DossieProjectoDataSet.Disciplina)

    End Sub

    Private Sub NomeLabel_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.DisciplinaBindingSource.MovePrevious()
        Button1.Enabled = False
        Button5.Enabled = True
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.DisciplinaBindingSource.MoveNext()
        Button1.Enabled = False
        Button5.Enabled = True

    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        Me.ModuloBindingSource.MoveNext()
        Button6.Enabled = True

    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        Me.ModuloBindingSource.MovePrevious()
        Button6.Enabled = True

    End Sub

    Private Sub Button10_Click(sender As Object, e As EventArgs) Handles Button10.Click

        ComboBox2.Text = ""
        NomeTextBox.Clear()
        ObjectivosTextBox.Clear()
        NumeroTextBox.Clear()
        HorasTextBox.Clear()
        Button6.Enabled = False

    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        CodDiscipTextBox.Clear()
        ComboBox1.Text = ""
        NomeTextBox1.Clear()
        Button1.Enabled = True
        Button5.Enabled = False




    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Try
            Me.DisciplinaTableAdapter.DeleteQuery1(CodDiscipTextBox.Text)
            Me.DisciplinaTableAdapter.Fill(Me.DossieProjectoDataSet.Disciplina)
        Catch ex As Exception
            MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Try
            Me.DisciplinaTableAdapter.InsertQuery(ComboBox1.SelectedValue, NomeTextBox1.Text)
            Me.DisciplinaBindingSource.EndEdit()
            Me.DisciplinaTableAdapter.Fill(Me.DossieProjectoDataSet.Disciplina)
            Me.DisciplinaBindingSource.MoveLast()

        Catch ex As Exception
            MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Try
            Me.ModuloTableAdapter.DeleteQuery(ComboBox2.SelectedValue)
            Me.ModuloTableAdapter.Fill(Me.DossieProjectoDataSet.Modulo)
            Me.ModuloBindingSource.MoveLast()

        Catch ex As Exception
            MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Me.ModuloTableAdapter.InsertQuery(ComboBox2.SelectedValue, NomeTextBox.Text, ObjectivosTextBox.Text, HorasTextBox.Text, NumeroTextBox.Text)
            Me.ModuloBindingSource.EndEdit()
            Me.ModuloTableAdapter.Fill(Me.DossieProjectoDataSet.Modulo)
            Me.ModuloBindingSource.MoveLast()
        Catch ex As Exception
            MessageBox.Show("Ocorreram erros na operação! > " & ex.Message(), "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ObjectivosTextBox_TextChanged(sender As Object, e As EventArgs) Handles ObjectivosTextBox.TextChanged

    End Sub

    Private Sub ComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedIndexChanged
        Me.ModuloTableAdapter.FillBy(Me.DossieProjectoDataSet.Modulo, ComboBox2.SelectedValue)
    End Sub
End Class