﻿Public Class Turmas


    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        inscreverTurma.ShowDialog()

    End Sub
    Private Sub Turmas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Turma' table. You can move, or remove it, as needed.
        Me.TurmaTableAdapter.Fill(Me.DossieProjectoDataSet.Turma)

    End Sub




    Private Sub TurmaDataGridView_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles TurmaDataGridView.CellContentClick
        If e.ColumnIndex = 5 Then

            Dim id As Integer = TurmaDataGridView.Rows(e.RowIndex).Cells(0).Value
            Dim result = MessageBox.Show("Deseja mesmo eleminar esta turma?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation)

            If (result = DialogResult.Yes) Then
                Try
                    Me.TurmaTableAdapter.DelQuery(id)
                    Me.TurmaTableAdapter.Fill(Me.DossieProjectoDataSet.Turma)
                    Me.Validate()
                    Me.TurmaBindingSource.EndEdit()
                    Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)
                Catch ex As Exception
                    MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            End If
        ElseIf e.ColumnIndex = 4 Then
            Dim id As Integer = TurmaDataGridView.Rows(e.RowIndex).Cells(0).Value
            TurmaAlunos.selectedTurma = id
            TurmaAlunos.ShowDialog()
        End If
    End Sub


    Private Sub searchTurmaTxt_GotFocus(sender As Object, e As EventArgs) Handles searchTurmaTxt.GotFocus
        If (searchTurmaTxt.Text = "Escreva aqui o que deseja pesquisar..") Then
            searchTurmaTxt.Text = ""
        End If
    End Sub

    Private Sub searchTurmaTxt_LostFocus(sender As Object, e As EventArgs) Handles searchTurmaTxt.LostFocus
        If (searchTurmaTxt.Text = "") Then
            searchTurmaTxt.Text = "Escreva aqui o que deseja pesquisar.."
        End If
    End Sub



    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Verificar se o utilizador escrevou algo para procurar'
        Dim search As String = searchTurmaTxt.Text
        If (Not search = "Escreva aqui o que deseja pesquisar..") Then


            'Verificar a opção escolhida para fazer a pesquisa'
            Dim searchOption As String = searchsel.SelectedIndex

            Select Case searchOption
                Case 0
                    Try

                        If (Not IsNumeric(search)) Then
                            MessageBox.Show("O codigo não é numerico", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Try
                        End If

                        Me.TurmaTableAdapter.searchCod(Me.DossieProjectoDataSet.Turma, CInt(search))
                    Catch ex As Exception
                        MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Case 1
                    Try

                        If (Not IsNumeric(search)) Then
                            MessageBox.Show("O ano não é numerico", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            Exit Try
                        End If

                        Me.TurmaTableAdapter.searchAno(Me.DossieProjectoDataSet.Turma, CInt(search))
                    Catch ex As Exception
                        MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Case 2
                    Try
                        Me.TurmaTableAdapter.searchLetra(Me.DossieProjectoDataSet.Turma, search)
                    Catch ex As Exception
                        MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Case 3
                    Try
                        Me.TurmaTableAdapter.searchCurso(Me.DossieProjectoDataSet.Turma, search)
                    Catch ex As Exception
                        MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try

                Case Else
                    'Mostra mensagem ao utilizador que necessita de selecionar uma opção de pesquisa'
                    MessageBox.Show("Percisa de escolher o campo em que vai efectuar a pesquisa.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    'Focar SearchBox'
                    If (DialogResult.OK) Then
                        searchsel.Focus()
                    End If

            End Select

        Else
            'Mostra mensagem ao utilizador que necessita de escrever alguma coisa primeiro'
            MessageBox.Show("Percisa de escrever informação na barra de pesquisa para a efectuar.", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information)

            'Focar SearchBox'
            If (DialogResult.OK) Then
                searchTurmaTxt.Focus()
            End If
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            'Validar e Guardar'
            Me.Validate()
            Me.TurmaBindingSource.EndEdit()
            Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)

            'Mostar Mensagem de sucesso'
            MessageBox.Show("Alterações guardadas com Sucesso", "Sucesso", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try
            Me.TurmaTableAdapter.Fill(Me.DossieProjectoDataSet.Turma)
            searchTurmaTxt.Text = "Escreva aqui o que deseja pesquisar.."
            searchsel.Text = "Por:"
        Catch ex As Exception
            MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub searchTurmaTxt_TextChanged(sender As Object, e As EventArgs) Handles searchTurmaTxt.TextChanged

    End Sub
End Class