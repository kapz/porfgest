﻿Public Class sumario

    Private Sub sumario_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Disciplina' table. You can move, or remove it, as needed.
        Me.DisciplinaTableAdapter.Fill(Me.DossieProjectoDataSet.Disciplina)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.TurmaFormated' table. You can move, or remove it, as needed.
        Me.TurmaFormatedTableAdapter.Fill(Me.DossieProjectoDataSet.TurmaFormated)

    End Sub

    Private Sub TurmaFormatedBindingNavigatorSaveItem_Click(sender As Object, e As EventArgs)
        Me.Validate()
        Me.TurmaFormatedBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.DossieProjectoDataSet)

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Try
            Me.SumarioTableAdapter.InsertQuery(TurmaDataComboBox.SelectedValue, NomeComboBox.SelectedValue, DateTimePicker1.Text, TextBox1.Text)
        Catch ex As Exception
            MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Me.SumarioBindingSource.MoveNext()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.SumarioBindingSource.MovePrevious()
    End Sub
End Class