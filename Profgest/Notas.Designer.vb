﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Notas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim NomeLabel As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.AreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DossieProjectoDataSet = New Profgest.DossieProjectoDataSet()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ObjectoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.AvaliacaoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObjectoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.ObjectoTableAdapter()
        Me.AreaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter()
        Me.DossieProjectoDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliacaoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AvaliacaoTableAdapter()
        Me.NotasObjectoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NotasObjectoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.NotasObjectoTableAdapter()
        Me.TableAdapterManager = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.NotasObjectoDataGridView = New System.Windows.Forms.DataGridView()
        Me.codObj = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.codAluno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nome = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.x = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.NomeComboBox = New System.Windows.Forms.ComboBox()
        Me.AlunoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObjectoNotaAlunoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObjectoNotaAlunoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.ObjectoNotaAlunoTableAdapter()
        Me.AlunoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AlunoTableAdapter()
        NomeLabel = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ObjectoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NotasObjectoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NotasObjectoDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlunoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ObjectoNotaAlunoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'NomeLabel
        '
        NomeLabel.AutoSize = True
        NomeLabel.Location = New System.Drawing.Point(9, 33)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(34, 13)
        NomeLabel.TabIndex = 121
        NomeLabel.Text = "Aluno"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Location = New System.Drawing.Point(203, 33)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(30, 13)
        Label7.TabIndex = 123
        Label7.Text = "Nota"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(189, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(150, 55)
        Me.Label2.TabIndex = 113
        Me.Label2.Text = "Notas"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(67, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 33)
        Me.Label1.TabIndex = 106
        Me.Label1.Text = "Profgest"
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox5.Image = Global.Profgest.My.Resources.Resources.help
        Me.PictureBox5.Location = New System.Drawing.Point(698, 7)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(67, 43)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 112
        Me.PictureBox5.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.a4f1f8f2_63f5_436d_b0d8_f325b11a22fc
        Me.PictureBox3.Location = New System.Drawing.Point(11, 63)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 110
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox4.Location = New System.Drawing.Point(-3, 57)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 111
        Me.PictureBox4.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(6, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 107
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(-6, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(785, 57)
        Me.PictureBox2.TabIndex = 108
        Me.PictureBox2.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        Me.PictureBox7.Location = New System.Drawing.Point(177, 52)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(602, 102)
        Me.PictureBox7.TabIndex = 109
        Me.PictureBox7.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.ListBox1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(29, 216)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(270, 364)
        Me.GroupBox1.TabIndex = 114
        Me.GroupBox1.TabStop = False
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.AreaBindingSource
        Me.ComboBox2.DisplayMember = "nome"
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(62, 62)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(192, 21)
        Me.ComboBox2.TabIndex = 117
        Me.ComboBox2.ValueMember = "codArea"
        '
        'AreaBindingSource
        '
        Me.AreaBindingSource.DataMember = "Area"
        Me.AreaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'DossieProjectoDataSet
        '
        Me.DossieProjectoDataSet.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(9, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(29, 13)
        Me.Label5.TabIndex = 117
        Me.Label5.Text = "Area"
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.ObjectoBindingSource
        Me.ListBox1.DisplayMember = "nome"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(12, 96)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(242, 251)
        Me.ListBox1.TabIndex = 118
        Me.ListBox1.ValueMember = "codObj"
        '
        'ObjectoBindingSource
        '
        Me.ObjectoBindingSource.DataMember = "Objecto"
        Me.ObjectoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label4.Location = New System.Drawing.Point(6, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(131, 33)
        Me.Label4.TabIndex = 117
        Me.Label4.Text = "Objectos"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 184)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 116
        Me.Label3.Text = "Avaliação"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.AvaliacaoBindingSource
        Me.ComboBox1.DisplayMember = "codAvaliacao"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(91, 180)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(60, 21)
        Me.ComboBox1.TabIndex = 115
        Me.ComboBox1.ValueMember = "codAvaliacao"
        '
        'AvaliacaoBindingSource
        '
        Me.AvaliacaoBindingSource.DataMember = "Avaliacao"
        Me.AvaliacaoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ObjectoTableAdapter
        '
        Me.ObjectoTableAdapter.ClearBeforeFill = True
        '
        'AreaTableAdapter
        '
        Me.AreaTableAdapter.ClearBeforeFill = True
        '
        'DossieProjectoDataSetBindingSource
        '
        Me.DossieProjectoDataSetBindingSource.DataSource = Me.DossieProjectoDataSet
        Me.DossieProjectoDataSetBindingSource.Position = 0
        '
        'AvaliacaoTableAdapter
        '
        Me.AvaliacaoTableAdapter.ClearBeforeFill = True
        '
        'NotasObjectoBindingSource
        '
        Me.NotasObjectoBindingSource.DataMember = "NotasObjecto"
        Me.NotasObjectoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'NotasObjectoTableAdapter
        '
        Me.NotasObjectoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager.AlunoTableAdapter = Nothing
        Me.TableAdapterManager.AreaTableAdapter = Me.AreaTableAdapter
        Me.TableAdapterManager.AvaliacaoTableAdapter = Me.AvaliacaoTableAdapter
        Me.TableAdapterManager.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DisciplinaTableAdapter = Nothing
        Me.TableAdapterManager.HelpdeskTableAdapter = Nothing
        Me.TableAdapterManager.ModuloTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoTableAdapter = Me.ObjectoTableAdapter
        Me.TableAdapterManager.SumarioTableAdapter = Nothing
        Me.TableAdapterManager.TurmaFormatedTableAdapter = Nothing
        Me.TableAdapterManager.TurmaTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'NotasObjectoDataGridView
        '
        Me.NotasObjectoDataGridView.AllowUserToAddRows = False
        Me.NotasObjectoDataGridView.AllowUserToDeleteRows = False
        Me.NotasObjectoDataGridView.AutoGenerateColumns = False
        Me.NotasObjectoDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.NotasObjectoDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.codObj, Me.codAluno, Me.DataGridViewTextBoxColumn1, Me.nome, Me.DataGridViewTextBoxColumn4, Me.x})
        Me.NotasObjectoDataGridView.DataSource = Me.NotasObjectoBindingSource
        Me.NotasObjectoDataGridView.Location = New System.Drawing.Point(6, 118)
        Me.NotasObjectoDataGridView.Name = "NotasObjectoDataGridView"
        Me.NotasObjectoDataGridView.Size = New System.Drawing.Size(444, 220)
        Me.NotasObjectoDataGridView.TabIndex = 116
        '
        'codObj
        '
        Me.codObj.DataPropertyName = "codObj"
        Me.codObj.HeaderText = "codObj"
        Me.codObj.Name = "codObj"
        Me.codObj.Visible = False
        '
        'codAluno
        '
        Me.codAluno.DataPropertyName = "codAluno"
        Me.codAluno.HeaderText = "codAluno"
        Me.codAluno.Name = "codAluno"
        Me.codAluno.Visible = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "numero"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Nº"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.Width = 50
        '
        'nome
        '
        Me.nome.DataPropertyName = "nome"
        Me.nome.HeaderText = "Aluno"
        Me.nome.Name = "nome"
        Me.nome.Width = 200
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Nota"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Nota"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.Width = 50
        '
        'x
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.IndianRed
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        Me.x.DefaultCellStyle = DataGridViewCellStyle3
        Me.x.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.x.HeaderText = ""
        Me.x.Name = "x"
        Me.x.Text = "x"
        Me.x.UseColumnTextForButtonValue = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.NotasObjectoDataGridView)
        Me.GroupBox2.Location = New System.Drawing.Point(316, 223)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(463, 357)
        Me.GroupBox2.TabIndex = 117
        Me.GroupBox2.TabStop = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label6.Location = New System.Drawing.Point(6, 82)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 33)
        Me.Label6.TabIndex = 119
        Me.Label6.Text = "Notas"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button2)
        Me.GroupBox3.Controls.Add(Me.NumericUpDown1)
        Me.GroupBox3.Controls.Add(Label7)
        Me.GroupBox3.Controls.Add(NomeLabel)
        Me.GroupBox3.Controls.Add(Me.NomeComboBox)
        Me.GroupBox3.Location = New System.Drawing.Point(316, 223)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(463, 76)
        Me.GroupBox3.TabIndex = 120
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Dar nota"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(325, 25)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(125, 28)
        Me.Button2.TabIndex = 125
        Me.Button2.Text = "Adicionar"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(250, 29)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {20, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(60, 20)
        Me.NumericUpDown1.TabIndex = 124
        '
        'NomeComboBox
        '
        Me.NomeComboBox.DataSource = Me.AlunoBindingSource
        Me.NomeComboBox.DisplayMember = "nome"
        Me.NomeComboBox.FormattingEnabled = True
        Me.NomeComboBox.Location = New System.Drawing.Point(49, 29)
        Me.NomeComboBox.Name = "NomeComboBox"
        Me.NomeComboBox.Size = New System.Drawing.Size(148, 21)
        Me.NomeComboBox.TabIndex = 122
        Me.NomeComboBox.ValueMember = "codAluno"
        '
        'AlunoBindingSource
        '
        Me.AlunoBindingSource.DataMember = "Aluno"
        Me.AlunoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ObjectoNotaAlunoBindingSource
        '
        Me.ObjectoNotaAlunoBindingSource.DataMember = "ObjectoNotaAluno"
        Me.ObjectoNotaAlunoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ObjectoNotaAlunoTableAdapter
        '
        Me.ObjectoNotaAlunoTableAdapter.ClearBeforeFill = True
        '
        'AlunoTableAdapter
        '
        Me.AlunoTableAdapter.ClearBeforeFill = True
        '
        'Notas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(777, 600)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox7)
        Me.Name = "Notas"
        Me.Text = "Notas"
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ObjectoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NotasObjectoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NotasObjectoDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.NumericUpDown1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlunoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ObjectoNotaAlunoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents ObjectoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ObjectoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.ObjectoTableAdapter
    Friend WithEvents AreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AreaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter
    Friend WithEvents DossieProjectoDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliacaoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliacaoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AvaliacaoTableAdapter
    Friend WithEvents NotasObjectoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NotasObjectoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.NotasObjectoTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents NotasObjectoDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents ObjectoNotaAlunoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ObjectoNotaAlunoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.ObjectoNotaAlunoTableAdapter
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents AlunoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AlunoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AlunoTableAdapter
    Friend WithEvents NomeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents NumericUpDown1 As System.Windows.Forms.NumericUpDown
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents codObj As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents codAluno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nome As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents x As System.Windows.Forms.DataGridViewButtonColumn
End Class
