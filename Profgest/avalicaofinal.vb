﻿Public Class avalicaofinal

    Private Sub avalicaofinal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Aluno' table. You can move, or remove it, as needed.
        Me.AlunoTableAdapter.Fill(Me.DossieProjectoDataSet.Aluno)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.AlunoNotaAvali' table. You can move, or remove it, as needed.
        Me.AlunoNotaAvaliTableAdapter.Fill(Me.DossieProjectoDataSet.AlunoNotaAvali)
        'TODO: This line of code loads data into the 'DossieProjectoDataSet.Avaliacao' table. You can move, or remove it, as needed.
        Me.AvaliacaoTableAdapter.Fill(Me.DossieProjectoDataSet.Avaliacao)

        If (avaliacao.codavaliacao_escolhida <> 0) Then
            ComboBox1.SelectedValue = avaliacao.codavaliacao_escolhida
            Dim turma = Me.AvaliacaoTableAdapter.ScalarQuery(avaliacao.codavaliacao_escolhida)
            Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, turma)
            Me.NotasFinaisTableAdapter.Fill(Me.DossieProjectoDataSet.NotasFinais, avaliacao.codavaliacao_escolhida)
            Me.AlunoNotaAvaliTableAdapter.FillBy(Me.DossieProjectoDataSet.AlunoNotaAvali, ComboBox1.SelectedValue)
        End If
    End Sub


    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Dim turma = Me.AvaliacaoTableAdapter.ScalarQuery(ComboBox1.SelectedValue)
        Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, turma)
        Me.NotasFinaisTableAdapter.Fill(Me.DossieProjectoDataSet.NotasFinais, ComboBox1.SelectedValue)
        Me.AlunoNotaAvaliTableAdapter.FillBy(Me.DossieProjectoDataSet.AlunoNotaAvali, ComboBox1.SelectedValue)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Try


            Me.AlunoNotaAvaliTableAdapter.InsertQuery(ComboBox1.SelectedValue, ComboBox2.SelectedValue, NumericUpDown1.Value, NumericUpDown2.Value, NumericUpDown3.Value)
            Me.AlunoNotaAvaliTableAdapter.FillBy(Me.DossieProjectoDataSet.AlunoNotaAvali, ComboBox1.SelectedValue)

            MessageBox.Show("Nota final inserida!", "Sucesso", MessageBoxButtons.OK)

            ComboBox2.Text = ""
            NumericUpDown1.Value = 0
            NumericUpDown2.Value = 0
            NumericUpDown3.Value = 0

        Catch ex As Exception
            MessageBox.Show("Internal Error: " & ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub avalicaofinal_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If (avaliacao.codavaliacao_escolhida <> 0) Then
            ComboBox1.SelectedValue = avaliacao.codavaliacao_escolhida
            Dim turma = Me.AvaliacaoTableAdapter.ScalarQuery(avaliacao.codavaliacao_escolhida)
            Me.AlunoTableAdapter.TurmaAlunos(Me.DossieProjectoDataSet.Aluno, turma)
            Me.NotasFinaisTableAdapter.Fill(Me.DossieProjectoDataSet.NotasFinais, avaliacao.codavaliacao_escolhida)
            Me.AlunoNotaAvaliTableAdapter.FillBy(Me.DossieProjectoDataSet.AlunoNotaAvali, ComboBox1.SelectedValue)
        End If
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        help.Show()

    End Sub
End Class