﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class sumario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim TurmaDataLabel As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim NomeLabel As System.Windows.Forms.Label
        Dim IDLabel As System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.TurmaDataComboBox = New System.Windows.Forms.ComboBox()
        Me.TurmaFormatedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DossieProjectoDataSet = New Profgest.DossieProjectoDataSet()
        Me.SumarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TurmaFormatedBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.DisciplinaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NomeComboBox = New System.Windows.Forms.ComboBox()
        Me.DisciplinaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.TurmaFormatedTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter()
        Me.TableAdapterManager = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.DisciplinaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.DisciplinaTableAdapter()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.SumarioTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.SumarioTableAdapter()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        TurmaDataLabel = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        NomeLabel = New System.Windows.Forms.Label()
        IDLabel = New System.Windows.Forms.Label()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SumarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DisciplinaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DisciplinaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TurmaDataLabel
        '
        TurmaDataLabel.AutoSize = True
        TurmaDataLabel.Location = New System.Drawing.Point(38, 171)
        TurmaDataLabel.Name = "TurmaDataLabel"
        TurmaDataLabel.Size = New System.Drawing.Size(49, 13)
        TurmaDataLabel.TabIndex = 48
        TurmaDataLabel.Text = "TURMA:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(337, 244)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(45, 13)
        Label4.TabIndex = 52
        Label4.Text = "Sumário"
        '
        'NomeLabel
        '
        NomeLabel.AutoSize = True
        NomeLabel.Location = New System.Drawing.Point(38, 208)
        NomeLabel.Name = "NomeLabel"
        NomeLabel.Size = New System.Drawing.Size(55, 13)
        NomeLabel.TabIndex = 53
        NomeLabel.Text = "Disciplina:"
        '
        'IDLabel
        '
        IDLabel.AutoSize = True
        IDLabel.Location = New System.Drawing.Point(589, 170)
        IDLabel.Name = "IDLabel"
        IDLabel.Size = New System.Drawing.Size(0, 13)
        IDLabel.TabIndex = 55
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Open Sans Light", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(198, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(229, 65)
        Me.Label2.TabIndex = 47
        Me.Label2.Text = "Sumários"
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Coral
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.sumarios
        Me.PictureBox3.Location = New System.Drawing.Point(9, 63)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 45
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.Coral
        Me.PictureBox4.Location = New System.Drawing.Point(-4, 55)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 46
        Me.PictureBox4.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(4, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 41
        Me.PictureBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Open Sans Light", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(65, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(126, 39)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Profgest"
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(-8, -1)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(731, 57)
        Me.PictureBox2.TabIndex = 42
        Me.PictureBox2.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        Me.PictureBox7.Location = New System.Drawing.Point(176, 55)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(549, 97)
        Me.PictureBox7.TabIndex = 44
        Me.PictureBox7.TabStop = False
        '
        'TurmaDataComboBox
        '
        Me.TurmaDataComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.TurmaFormatedBindingSource, "TurmaData", True))
        Me.TurmaDataComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.SumarioBindingSource, "codturma", True))
        Me.TurmaDataComboBox.DataSource = Me.TurmaFormatedBindingSource1
        Me.TurmaDataComboBox.DisplayMember = "TurmaData"
        Me.TurmaDataComboBox.FormattingEnabled = True
        Me.TurmaDataComboBox.Location = New System.Drawing.Point(96, 163)
        Me.TurmaDataComboBox.Name = "TurmaDataComboBox"
        Me.TurmaDataComboBox.Size = New System.Drawing.Size(124, 21)
        Me.TurmaDataComboBox.TabIndex = 49
        Me.TurmaDataComboBox.ValueMember = "codTurma"
        '
        'TurmaFormatedBindingSource
        '
        Me.TurmaFormatedBindingSource.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'DossieProjectoDataSet
        '
        Me.DossieProjectoDataSet.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SumarioBindingSource
        '
        Me.SumarioBindingSource.DataMember = "Sumario"
        Me.SumarioBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'TurmaFormatedBindingSource1
        '
        Me.TurmaFormatedBindingSource1.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SumarioBindingSource, "data", True))
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Tag", Me.SumarioBindingSource, "data", True))
        Me.DateTimePicker1.DataBindings.Add(New System.Windows.Forms.Binding("Value", Me.SumarioBindingSource, "data", True))
        Me.DateTimePicker1.Location = New System.Drawing.Point(490, 171)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 50
        '
        'TextBox1
        '
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.SumarioBindingSource, "texto", True))
        Me.TextBox1.DataBindings.Add(New System.Windows.Forms.Binding("Tag", Me.SumarioBindingSource, "texto", True))
        Me.TextBox1.Location = New System.Drawing.Point(41, 260)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(649, 193)
        Me.TextBox1.TabIndex = 53
        '
        'DisciplinaBindingSource
        '
        Me.DisciplinaBindingSource.DataMember = "TurmaDisciplina1"
        Me.DisciplinaBindingSource.DataSource = Me.TurmaFormatedBindingSource
        '
        'NomeComboBox
        '
        Me.NomeComboBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.DisciplinaBindingSource, "nome", True))
        Me.NomeComboBox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.SumarioBindingSource, "coddisciplina", True))
        Me.NomeComboBox.DataSource = Me.DisciplinaBindingSource1
        Me.NomeComboBox.DisplayMember = "nome"
        Me.NomeComboBox.FormattingEnabled = True
        Me.NomeComboBox.Location = New System.Drawing.Point(96, 204)
        Me.NomeComboBox.Name = "NomeComboBox"
        Me.NomeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.NomeComboBox.TabIndex = 54
        Me.NomeComboBox.ValueMember = "codDiscip"
        '
        'DisciplinaBindingSource1
        '
        Me.DisciplinaBindingSource1.DataMember = "Disciplina"
        Me.DisciplinaBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'TurmaFormatedTableAdapter
        '
        Me.TurmaFormatedTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager.AlunoTableAdapter = Nothing
        Me.TableAdapterManager.AreaTableAdapter = Nothing
        Me.TableAdapterManager.AvaliacaoTableAdapter = Nothing
        Me.TableAdapterManager.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DisciplinaTableAdapter = Me.DisciplinaTableAdapter
        Me.TableAdapterManager.ModuloTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoTableAdapter = Nothing
        Me.TableAdapterManager.SumarioTableAdapter = Nothing
        Me.TableAdapterManager.TurmaFormatedTableAdapter = Me.TurmaFormatedTableAdapter
        Me.TableAdapterManager.TurmaTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'DisciplinaTableAdapter
        '
        Me.DisciplinaTableAdapter.ClearBeforeFill = True
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(41, 459)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(649, 53)
        Me.Button5.TabIndex = 55
        Me.Button5.Text = "Adicionar"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button5.UseVisualStyleBackColor = False
        '
        'SumarioTableAdapter
        '
        Me.SumarioTableAdapter.ClearBeforeFill = True
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button4.ForeColor = System.Drawing.Color.White
        Me.Button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button4.Location = New System.Drawing.Point(659, 197)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(30, 33)
        Me.Button4.TabIndex = 75
        Me.Button4.Text = ">"
        Me.Button4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Open Sans Light", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(623, 197)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(30, 33)
        Me.Button3.TabIndex = 74
        Me.Button3.Text = "<"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.UseVisualStyleBackColor = False
        '
        'sumario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(723, 532)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(IDLabel)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(NomeLabel)
        Me.Controls.Add(Me.NomeComboBox)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(TurmaDataLabel)
        Me.Controls.Add(Me.TurmaDataComboBox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox7)
        Me.Name = "sumario"
        Me.Text = "sumario"
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SumarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DisciplinaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DisciplinaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents TurmaFormatedBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TurmaFormatedTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents DisciplinaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.DisciplinaTableAdapter
    Friend WithEvents TurmaDataComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents DisciplinaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NomeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents SumarioBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TurmaFormatedBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents DisciplinaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents SumarioTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.SumarioTableAdapter
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
End Class
