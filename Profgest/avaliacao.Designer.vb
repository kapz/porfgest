﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class avaliacao
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim CodTurmaLabel As System.Windows.Forms.Label
        Dim CodMuduloLabel As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ModuloBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DossieProjectoDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DossieProjectoDataSet = New Profgest.DossieProjectoDataSet()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.ModuloAvaliacaoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AlunoNotaAvaliBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliacaoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliacaoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AvaliacaoTableAdapter()
        Me.TableAdapterManager = New Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager()
        Me.ModuloTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.ModuloTableAdapter()
        Me.TurmaFormatedTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter()
        Me.TurmaFormatedBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliaçãoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TurmaFormatedBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ModuloBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AlunoNotaAvaliTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AlunoNotaAvaliTableAdapter()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ModuloBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TurmaFormatedBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliAreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliAreaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AvaliAreaTableAdapter()
        Me.AreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliacaoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AreaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter()
        Me.AvaliacaoAvaliAreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Areas = New System.Windows.Forms.CheckedListBox()
        Me.disciplina = New System.Windows.Forms.ComboBox()
        Me.DisciplinaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.ObjectoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObjectoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.ObjectoTableAdapter()
        Me.AvaliacaoBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AreaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AreaAvaliAreaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvaliAreaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AreaAvaliAreaBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.AreaAvaliAreaBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObjectoNotaAlunoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ObjectoNotaAlunoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.ObjectoNotaAlunoTableAdapter()
        Me.AlunoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AlunoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.AlunoTableAdapter()
        Me.NotasObjectoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.NotasObjectoTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.NotasObjectoTableAdapter()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Avalicao2DataGridView = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Objectos = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.X = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.Avalicao2BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Avalicao2TableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.Avalicao2TableAdapter()
        Me.DisciplinaTableAdapter = New Profgest.DossieProjectoDataSetTableAdapters.DisciplinaTableAdapter()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        CodTurmaLabel = New System.Windows.Forms.Label()
        CodMuduloLabel = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        CType(Me.ModuloBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModuloAvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlunoNotaAvaliBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliaçãoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModuloBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ModuloBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TurmaFormatedBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliacaoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliacaoAvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DisciplinaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ObjectoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliacaoBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AreaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AreaAvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvaliAreaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AreaAvaliAreaBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AreaAvaliAreaBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ObjectoNotaAlunoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AlunoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NotasObjectoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.Avalicao2DataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Avalicao2BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CodTurmaLabel
        '
        CodTurmaLabel.AutoSize = True
        CodTurmaLabel.Location = New System.Drawing.Point(20, 80)
        CodTurmaLabel.Name = "CodTurmaLabel"
        CodTurmaLabel.Size = New System.Drawing.Size(40, 13)
        CodTurmaLabel.TabIndex = 31
        CodTurmaLabel.Text = "Turma:"
        '
        'CodMuduloLabel
        '
        CodMuduloLabel.AutoSize = True
        CodMuduloLabel.Location = New System.Drawing.Point(20, 134)
        CodMuduloLabel.Name = "CodMuduloLabel"
        CodMuduloLabel.Size = New System.Drawing.Size(45, 13)
        CodMuduloLabel.TabIndex = 33
        CodMuduloLabel.Text = "Modulo:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Location = New System.Drawing.Point(20, 107)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(55, 13)
        Label8.TabIndex = 78
        Label8.Text = "Disciplina:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(23, 172)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(37, 13)
        Label5.TabIndex = 81
        Label5.Text = "Areas:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Label1.Location = New System.Drawing.Point(70, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 33)
        Me.Label1.TabIndex = 21
        Me.Label1.Text = "Profgest"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.White
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(203, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(256, 55)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Avaliações"
        '
        'ModuloBindingSource
        '
        Me.ModuloBindingSource.DataMember = "Modulo"
        Me.ModuloBindingSource.DataSource = Me.DossieProjectoDataSetBindingSource
        '
        'DossieProjectoDataSetBindingSource
        '
        Me.DossieProjectoDataSetBindingSource.DataSource = Me.DossieProjectoDataSet
        Me.DossieProjectoDataSetBindingSource.Position = 0
        '
        'DossieProjectoDataSet
        '
        Me.DossieProjectoDataSet.DataSetName = "DossieProjectoDataSet"
        Me.DossieProjectoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox3.Image = Global.Profgest.My.Resources.Resources.a4f1f8f2_63f5_436d_b0d8_f325b11a22fc
        Me.PictureBox3.Location = New System.Drawing.Point(14, 63)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(161, 76)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 26
        Me.PictureBox3.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.BackColor = System.Drawing.Color.DarkOrchid
        Me.PictureBox4.Location = New System.Drawing.Point(1, 56)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(186, 97)
        Me.PictureBox4.TabIndex = 27
        Me.PictureBox4.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox1.Image = Global.Profgest.My.Resources.Resources.logo
        Me.PictureBox1.Location = New System.Drawing.Point(9, 7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(55, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 22
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox2.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(1034, 57)
        Me.PictureBox2.TabIndex = 23
        Me.PictureBox2.TabStop = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button5.ForeColor = System.Drawing.Color.White
        Me.Button5.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button5.Location = New System.Drawing.Point(646, 63)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(213, 82)
        Me.Button5.TabIndex = 20
        Me.Button5.Text = "Disciplinas e Modulos"
        Me.Button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button5.UseVisualStyleBackColor = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.White
        Me.PictureBox7.Location = New System.Drawing.Point(181, 51)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(850, 102)
        Me.PictureBox7.TabIndex = 25
        Me.PictureBox7.TabStop = False
        '
        'ModuloAvaliacaoBindingSource
        '
        Me.ModuloAvaliacaoBindingSource.DataMember = "ModuloAvaliacao"
        Me.ModuloAvaliacaoBindingSource.DataSource = Me.ModuloBindingSource
        '
        'AlunoNotaAvaliBindingSource
        '
        Me.AlunoNotaAvaliBindingSource.DataMember = "AvaliacaoAlunoNotaAvali"
        Me.AlunoNotaAvaliBindingSource.DataSource = Me.AvaliacaoBindingSource
        '
        'AvaliacaoBindingSource
        '
        Me.AvaliacaoBindingSource.DataMember = "Avaliacao"
        Me.AvaliacaoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'AvaliacaoTableAdapter
        '
        Me.AvaliacaoTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.AlunoNotaAvaliTableAdapter = Nothing
        Me.TableAdapterManager.AlunoTableAdapter = Nothing
        Me.TableAdapterManager.AreaTableAdapter = Nothing
        Me.TableAdapterManager.AvaliacaoTableAdapter = Me.AvaliacaoTableAdapter
        Me.TableAdapterManager.AvaliAreaTableAdapter = Nothing
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.DisciplinaTableAdapter = Nothing
        Me.TableAdapterManager.HelpdeskTableAdapter = Nothing
        Me.TableAdapterManager.ModuloTableAdapter = Me.ModuloTableAdapter
        Me.TableAdapterManager.ObjectoNotaAlunoTableAdapter = Nothing
        Me.TableAdapterManager.ObjectoTableAdapter = Nothing
        Me.TableAdapterManager.SumarioTableAdapter = Nothing
        Me.TableAdapterManager.TurmaFormatedTableAdapter = Me.TurmaFormatedTableAdapter
        Me.TableAdapterManager.TurmaTableAdapter = Nothing
        Me.TableAdapterManager.UpdateOrder = Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ModuloTableAdapter
        '
        Me.ModuloTableAdapter.ClearBeforeFill = True
        '
        'TurmaFormatedTableAdapter
        '
        Me.TurmaFormatedTableAdapter.ClearBeforeFill = True
        '
        'TurmaFormatedBindingSource
        '
        Me.TurmaFormatedBindingSource.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'TurmaFormatedBindingSource1
        '
        Me.TurmaFormatedBindingSource1.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'ModuloBindingSource1
        '
        Me.ModuloBindingSource1.DataMember = "Modulo"
        Me.ModuloBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'AlunoNotaAvaliTableAdapter
        '
        Me.AlunoNotaAvaliTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button1.Location = New System.Drawing.Point(865, 64)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(154, 82)
        Me.Button1.TabIndex = 29
        Me.Button1.Text = "Areas"
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button1.UseVisualStyleBackColor = False
        '
        'ModuloBindingSource2
        '
        Me.ModuloBindingSource2.DataMember = "Modulo"
        Me.ModuloBindingSource2.DataSource = Me.DossieProjectoDataSet
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label3.Location = New System.Drawing.Point(6, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(215, 33)
        Me.Label3.TabIndex = 70
        Me.Label3.Text = "Nova avaliação"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.MediumAquamarine
        Me.Button2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button2.Enabled = False
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Image = Global.Profgest.My.Resources.Resources.add2
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(103, 314)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(144, 50)
        Me.Button2.TabIndex = 71
        Me.Button2.Text = "Adicionar"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = False
        '
        'TurmaFormatedBindingSource2
        '
        Me.TurmaFormatedBindingSource2.DataMember = "TurmaFormated"
        Me.TurmaFormatedBindingSource2.DataSource = Me.DossieProjectoDataSet
        '
        'AvaliAreaBindingSource
        '
        Me.AvaliAreaBindingSource.DataMember = "AvaliArea"
        Me.AvaliAreaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'AvaliAreaTableAdapter
        '
        Me.AvaliAreaTableAdapter.ClearBeforeFill = True
        '
        'AreaBindingSource
        '
        Me.AreaBindingSource.DataMember = "Area"
        Me.AreaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'AvaliacaoBindingSource1
        '
        Me.AvaliacaoBindingSource1.DataMember = "Avaliacao"
        Me.AvaliacaoBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'AreaTableAdapter
        '
        Me.AreaTableAdapter.ClearBeforeFill = True
        '
        'AvaliacaoAvaliAreaBindingSource
        '
        Me.AvaliacaoAvaliAreaBindingSource.DataMember = "AvaliacaoAvaliArea"
        Me.AvaliacaoAvaliAreaBindingSource.DataSource = Me.AvaliacaoBindingSource
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Label5)
        Me.GroupBox1.Controls.Add(Me.Areas)
        Me.GroupBox1.Controls.Add(Me.disciplina)
        Me.GroupBox1.Controls.Add(Label8)
        Me.GroupBox1.Controls.Add(Me.ComboBox2)
        Me.GroupBox1.Controls.Add(Me.ComboBox1)
        Me.GroupBox1.Controls.Add(Me.Button6)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(CodTurmaLabel)
        Me.GroupBox1.Controls.Add(CodMuduloLabel)
        Me.GroupBox1.Location = New System.Drawing.Point(48, 173)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(260, 379)
        Me.GroupBox1.TabIndex = 80
        Me.GroupBox1.TabStop = False
        '
        'Areas
        '
        Me.Areas.FormattingEnabled = True
        Me.Areas.Location = New System.Drawing.Point(23, 197)
        Me.Areas.Name = "Areas"
        Me.Areas.Size = New System.Drawing.Size(222, 79)
        Me.Areas.TabIndex = 80
        '
        'disciplina
        '
        Me.disciplina.DataSource = Me.DisciplinaBindingSource
        Me.disciplina.DisplayMember = "nome"
        Me.disciplina.FormattingEnabled = True
        Me.disciplina.Location = New System.Drawing.Point(86, 104)
        Me.disciplina.Name = "disciplina"
        Me.disciplina.Size = New System.Drawing.Size(161, 21)
        Me.disciplina.TabIndex = 79
        Me.disciplina.ValueMember = "codDiscip"
        '
        'DisciplinaBindingSource
        '
        Me.DisciplinaBindingSource.DataMember = "Disciplina"
        Me.DisciplinaBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ComboBox2
        '
        Me.ComboBox2.DataSource = Me.ModuloBindingSource
        Me.ComboBox2.DisplayMember = "nome"
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(86, 134)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(161, 21)
        Me.ComboBox2.TabIndex = 76
        Me.ComboBox2.ValueMember = "codModulo"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.TurmaFormatedBindingSource
        Me.ComboBox1.DisplayMember = "TurmaData"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(86, 77)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(161, 21)
        Me.ComboBox1.TabIndex = 75
        Me.ComboBox1.ValueMember = "codTurma"
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.DarkOrange
        Me.Button6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button6.ForeColor = System.Drawing.Color.White
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.Location = New System.Drawing.Point(23, 314)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(74, 50)
        Me.Button6.TabIndex = 72
        Me.Button6.Text = "Novo"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'ObjectoBindingSource
        '
        Me.ObjectoBindingSource.DataMember = "Objecto"
        Me.ObjectoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ObjectoTableAdapter
        '
        Me.ObjectoTableAdapter.ClearBeforeFill = True
        '
        'AvaliacaoBindingSource2
        '
        Me.AvaliacaoBindingSource2.DataMember = "Avaliacao"
        Me.AvaliacaoBindingSource2.DataSource = Me.DossieProjectoDataSet
        '
        'AreaBindingSource1
        '
        Me.AreaBindingSource1.DataMember = "Area"
        Me.AreaBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'AreaAvaliAreaBindingSource
        '
        Me.AreaAvaliAreaBindingSource.DataMember = "AreaAvaliArea"
        Me.AreaAvaliAreaBindingSource.DataSource = Me.AreaBindingSource
        '
        'AvaliAreaBindingSource1
        '
        Me.AvaliAreaBindingSource1.DataMember = "AvaliArea"
        Me.AvaliAreaBindingSource1.DataSource = Me.DossieProjectoDataSet
        '
        'AreaAvaliAreaBindingSource1
        '
        Me.AreaAvaliAreaBindingSource1.DataMember = "AreaAvaliArea"
        Me.AreaAvaliAreaBindingSource1.DataSource = Me.AreaBindingSource
        '
        'AreaAvaliAreaBindingSource2
        '
        Me.AreaAvaliAreaBindingSource2.DataMember = "AreaAvaliArea"
        Me.AreaAvaliAreaBindingSource2.DataSource = Me.AreaBindingSource
        '
        'ObjectoNotaAlunoBindingSource
        '
        Me.ObjectoNotaAlunoBindingSource.DataMember = "ObjectoNotaAluno"
        Me.ObjectoNotaAlunoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'ObjectoNotaAlunoTableAdapter
        '
        Me.ObjectoNotaAlunoTableAdapter.ClearBeforeFill = True
        '
        'AlunoBindingSource
        '
        Me.AlunoBindingSource.DataMember = "Aluno"
        Me.AlunoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'AlunoTableAdapter
        '
        Me.AlunoTableAdapter.ClearBeforeFill = True
        '
        'NotasObjectoBindingSource
        '
        Me.NotasObjectoBindingSource.DataMember = "NotasObjecto"
        Me.NotasObjectoBindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'NotasObjectoTableAdapter
        '
        Me.NotasObjectoTableAdapter.ClearBeforeFill = True
        '
        'Button18
        '
        Me.Button18.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button18.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button18.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button18.ForeColor = System.Drawing.Color.White
        Me.Button18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button18.Location = New System.Drawing.Point(235, 59)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(238, 39)
        Me.Button18.TabIndex = 94
        Me.Button18.Text = "Notas"
        Me.Button18.UseVisualStyleBackColor = False
        '
        'Button19
        '
        Me.Button19.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button19.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button19.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button19.ForeColor = System.Drawing.Color.White
        Me.Button19.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button19.Location = New System.Drawing.Point(476, 59)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(210, 39)
        Me.Button19.TabIndex = 95
        Me.Button19.Text = "Avaliações finais"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button3)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.Button19)
        Me.GroupBox4.Controls.Add(Me.Avalicao2DataGridView)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.Button18)
        Me.GroupBox4.Location = New System.Drawing.Point(320, 173)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(686, 379)
        Me.GroupBox4.TabIndex = 96
        Me.GroupBox4.TabStop = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.DarkOrchid
        Me.Button3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.ForeColor = System.Drawing.Color.White
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(0, 59)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(232, 39)
        Me.Button3.TabIndex = 97
        Me.Button3.Text = "Objectos de avaliação"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(477, 28)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(197, 13)
        Me.Label9.TabIndex = 96
        Me.Label9.Text = "Selecione a avaliação abaixo e aceda a"
        '
        'Avalicao2DataGridView
        '
        Me.Avalicao2DataGridView.AllowUserToAddRows = False
        Me.Avalicao2DataGridView.AllowUserToDeleteRows = False
        Me.Avalicao2DataGridView.AutoGenerateColumns = False
        Me.Avalicao2DataGridView.BackgroundColor = System.Drawing.SystemColors.Control
        Me.Avalicao2DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.Avalicao2DataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.DataGridViewTextBoxColumn6, Me.DataGridViewTextBoxColumn7, Me.Objectos, Me.X})
        Me.Avalicao2DataGridView.DataSource = Me.Avalicao2BindingSource
        Me.Avalicao2DataGridView.Location = New System.Drawing.Point(0, 104)
        Me.Avalicao2DataGridView.Name = "Avalicao2DataGridView"
        Me.Avalicao2DataGridView.ReadOnly = True
        Me.Avalicao2DataGridView.Size = New System.Drawing.Size(686, 260)
        Me.Avalicao2DataGridView.TabIndex = 82
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "codAvaliacao"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Cod"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 40
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Turma"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Turma"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 50
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "NomeDisciplina"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Disciplina"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "NºModulo"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Nº Mod."
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "NomeModulo"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Modulo"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "ModuloHoras"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Horas"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        '
        'Objectos
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.White
        Me.Objectos.DefaultCellStyle = DataGridViewCellStyle3
        Me.Objectos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Objectos.HeaderText = ""
        Me.Objectos.Name = "Objectos"
        Me.Objectos.ReadOnly = True
        Me.Objectos.Text = "Ver Resultados"
        Me.Objectos.UseColumnTextForButtonValue = True
        '
        'X
        '
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.IndianRed
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.White
        Me.X.DefaultCellStyle = DataGridViewCellStyle4
        Me.X.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.X.HeaderText = ""
        Me.X.Name = "X"
        Me.X.ReadOnly = True
        Me.X.Text = "X"
        Me.X.UseColumnTextForButtonValue = True
        Me.X.Width = 50
        '
        'Avalicao2BindingSource
        '
        Me.Avalicao2BindingSource.DataMember = "Avalicao2"
        Me.Avalicao2BindingSource.DataSource = Me.DossieProjectoDataSet
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(6, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(157, 33)
        Me.Label7.TabIndex = 82
        Me.Label7.Text = "Avaliações"
        '
        'Avalicao2TableAdapter
        '
        Me.Avalicao2TableAdapter.ClearBeforeFill = True
        '
        'DisciplinaTableAdapter
        '
        Me.DisciplinaTableAdapter.ClearBeforeFill = True
        '
        'PictureBox5
        '
        Me.PictureBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer), CType(CType(51, Byte), Integer))
        Me.PictureBox5.Image = Global.Profgest.My.Resources.Resources.help
        Me.PictureBox5.Location = New System.Drawing.Point(952, 7)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(67, 43)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox5.TabIndex = 97
        Me.PictureBox5.TabStop = False
        '
        'avaliacao
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(1031, 577)
        Me.Controls.Add(Me.PictureBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox4)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.PictureBox7)
        Me.Name = "avaliacao"
        Me.Text = "avaliacao"
        CType(Me.ModuloBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DossieProjectoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModuloAvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlunoNotaAvaliBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliacaoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliaçãoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModuloBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ModuloBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TurmaFormatedBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliacaoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliacaoAvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DisciplinaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ObjectoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliacaoBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AreaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AreaAvaliAreaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvaliAreaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AreaAvaliAreaBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AreaAvaliAreaBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ObjectoNotaAlunoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AlunoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NotasObjectoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.Avalicao2DataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Avalicao2BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DossieProjectoDataSet As Profgest.DossieProjectoDataSet
    Friend WithEvents AvaliacaoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliacaoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AvaliacaoTableAdapter
    Friend WithEvents TableAdapterManager As Profgest.DossieProjectoDataSetTableAdapters.TableAdapterManager
    Friend WithEvents TurmaFormatedTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.TurmaFormatedTableAdapter
    Friend WithEvents TurmaFormatedBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliaçãoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModuloTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.ModuloTableAdapter
    Friend WithEvents DossieProjectoDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModuloBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ModuloBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents TurmaFormatedBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents ModuloAvaliacaoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AlunoNotaAvaliBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AlunoNotaAvaliTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AlunoNotaAvaliTableAdapter
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents ModuloBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TurmaFormatedBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliAreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliAreaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AvaliAreaTableAdapter
    Friend WithEvents AreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AreaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AreaTableAdapter
    Friend WithEvents AvaliacaoBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliacaoAvaliAreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents ObjectoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ObjectoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.ObjectoTableAdapter
    Friend WithEvents AvaliacaoBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents AvaliAreaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents AreaAvaliAreaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AreaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents AreaAvaliAreaBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents ComboBox2 As System.Windows.Forms.ComboBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents AreaAvaliAreaBindingSource2 As System.Windows.Forms.BindingSource
    Friend WithEvents AlunosObjectosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ObjectoNotaAlunoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ObjectoNotaAlunoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.ObjectoNotaAlunoTableAdapter
    Friend WithEvents AlunoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AlunoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.AlunoTableAdapter
    Friend WithEvents NotasObjectoBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents NotasObjectoTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.NotasObjectoTableAdapter
    Friend WithEvents disciplina As System.Windows.Forms.ComboBox
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Areas As System.Windows.Forms.CheckedListBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Avalicao2BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Avalicao2TableAdapter As Profgest.DossieProjectoDataSetTableAdapters.Avalicao2TableAdapter
    Friend WithEvents Avalicao2DataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents DisciplinaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DisciplinaTableAdapter As Profgest.DossieProjectoDataSetTableAdapters.DisciplinaTableAdapter
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Objectos As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents X As System.Windows.Forms.DataGridViewButtonColumn
End Class
